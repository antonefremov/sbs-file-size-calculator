package main

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/logger"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/config"
)

var (
	commitHash string
)

func main() {
	log.Info().
		Str("commitHash", commitHash).
		Msg("initialising service")

	cfg, err := config.NewConfig(commitHash)
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("unable to load service config")
	}

	l := logger.Init(cfg)
	l.Debug().
		EmbedObject(cfg).
		Msg("setting up server")

	s := internal.SetupServer(l, cfg)
	l.Info().
		Str(logger.KeyAPIVersion, cfg.APIVersion()).
		Msg("starting service")
	s.Start()
}
