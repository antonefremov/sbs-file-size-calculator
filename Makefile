.PHONY: build
build: go-build ## Build Go code

.PHONY: test
test: go-test ## Test Go code

.PHONY: test-cov
test-cov: go-test-cov ## Test Go code (with coverage)

.PHONY: cov-view
cov-view: go-cov-view ## View Go code coverage

.PHONY: cov-report
cov-report: go-cov-report ## Print a Go code coverage report to stdout

.PHONY: gen-mocks
gen-mocks: go-gen-mocks ## Generate mocks

.PHONY: lint
lint: go-lint ## Perform lint checks on code, updates files

.PHONY: validate
validate: cfn-validate ## Verify the cloudformation templates are valid

.PHONY: run
run: go-run ## Run Go code

.PHONY: debug
debug: go-debug ## Debug Go code using dlv + vscode

.PHONY: dbuild
dbuild: docker-build ## Build Docker image

.PHONY: drun
drun: docker-run ## Run Docker container

.PHONY: deploy
deploy: cfn-infrastructure-deploy cfn-service-deploy ## Deploy to ECS

.PHONY: help
help:
	@# Explicitly grep ./Makefile to avoid any included config files
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' ./Makefile | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

include config.mk

COL_RED=$(shell [ -n "${TERM}" ] && tput setaf 1)
COL_GREEN=$(shell [ -n "${TERM}" ] && tput setaf 2)
COL_CYAN=$(shell [ -n "${TERM}" ] && tput setaf 6)
COL_GREY=$(shell [ -n "${TERM}" ] && tput setaf 8)
COL_RESET=$(shell [ -n "${TERM}" ] && tput sgr0)

ifdef CI_COMMIT_SHORT_SHA
	COMMIT_HASH = ${CI_COMMIT_SHORT_SHA}
else
	COMMIT_HASH = $(shell git rev-parse --short HEAD)
endif

ifdef CI_ENVIRONMENT_NAME
    ENVIRONMENT = ${CI_ENVIRONMENT_NAME}
endif

go-build:
	@echo "Building Go code..."
	@rm -rf build
	@mkdir build
	export CGO_ENABLED=0 ; \
	export GOOS=linux ; export GOARCH=amd64 ; \
	export GOPRIVATE="gitlab.com/antonefremov" ; \
	go build -v -ldflags "-X main.commitHash=${COMMIT_HASH}" -o ../build/${SERVICE_NAME}
	@echo "Build assets stored to build/${SERVICE_NAME}. Use 'make run' to run service."

go-test:
	@echo "Running tests..."
	@go test -v ./... 2>&1 | \
		sed -lE "/(.*)PASS(.*)/s//\1$(COL_GREEN)PASS$(COL_RESET)\2/ ; /(.*)FAIL(.*)/s//\1$(COL_RED)FAIL$(COL_RESET)\2/ ; /=== (RUN.*)/s//=== $(COL_CYAN)\1$(COL_RESET)/ ; /(\?.*)/s//$(COL_GREY)\1$(COL_RESET)/ ; /^(#.*)/s//$(COL_RED)\1$(COL_RESET)/"

go-test-cov:
	@echo "Running tests..."
	go test -cover -covermode=count -coverprofile=coverage.out ./...
	@echo "Coverage report stored to coverage.out. Run 'make cov-view' to view report."

go-cov-view:
	go tool cover -html=coverage.out

go-cov-report:
	go tool cover -func=coverage.out

go-gen-mocks:
	@rm -rf $(shell find . -type d -name mocks) # Clean mocks
	go generate ./...

go-run:
	eval `cat ./config/local.cfg` go run .

go-debug:
	eval `cat ./config/local.cfg` dlv debug . --headless --listen=:2345 --log --api-version=2 -- auth

go-lint:
	# @go get github.com/golangci/golangci-lint/cmd/golangci-lint
	@go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.50.1
	# golangci-lint run
	golangci-lint run --disable-all -E errcheck

docker-build:
	echo "-> Running docker-build target"
	docker build --tag ${SERVICE_NAME} --build-arg serviceName=${SERVICE_NAME} .

docker-run: docker-build
	docker run -p ${PORT}:${PORT} \
			-e SERVICE_NAME=${SERVICE_NAME} \
			-e VERSION=${API_VERSION} \
			-e HTTP_PORT=${PORT} \
			--env-file ./config/local.cfg \
		${SERVICE_NAME}

docker-tag: docker-build
	@echo "-> Tagging docker image for deployment to AWS"
	docker tag ${SERVICE_NAME} ${CONTAINER_REGISTRY}/${REPOSITORY_NAME}:${COMMIT_HASH}
	docker tag ${SERVICE_NAME} ${CONTAINER_REGISTRY}/${REPOSITORY_NAME}:latest

docker-set-aws-credentials:
	aws ecr get-login-password \
	--region ${AWS_REGION} | docker login --username AWS --password-stdin ${CONTAINER_REGISTRY}

docker-push: docker-tag docker-set-aws-credentials cfn-service-shared-deploy
	docker push --all-tags ${CONTAINER_REGISTRY}/${REPOSITORY_NAME}

cfn-infrastructure-package:
	aws cloudformation package \
		--template-file cloudformation/infrastructure.yaml \
		--s3-bucket ${S3_BUCKET_NAME} \
		--s3-prefix ${SERVICE_NAME}-infrastructure \
		--output-template-file build/infrastructure.yaml

cfn-infrastructure-deploy: cfn-infrastructure-package
	$(eval CFN_PARAMS := $(shell cat ./config/${ENVIRONMENT}/infrastructure.cfg))
	aws cloudformation deploy \
		--template-file build/infrastructure.yaml \
		--stack-name ${INFRASTRUCTURE_STACK_NAME} \
		--capabilities CAPABILITY_NAMED_IAM \
		--parameter-overrides \
			${PROJECT_CFN_PARAMS} \
			${CFN_PARAMS} \
		--tags ${CFN_TAGS} \
		--region ${AWS_REGION}

cfn-service-shared-package:
	aws cloudformation package \
		--template-file cloudformation/service-shared.yaml \
		--s3-bucket ${S3_BUCKET_NAME} \
		--output-template-file build/service-shared.yaml

cfn-service-shared-deploy: cfn-service-shared-package
	aws cloudformation deploy \
	--template-file build/service-shared.yaml \
	--stack-name ${SERVICE_SHARED_STACK_NAME} \
	--capabilities CAPABILITY_NAMED_IAM \
	--parameter-overrides \
		${PROJECT_CFN_PARAMS}

cfn-service-package:
	aws cloudformation package \
		--template-file cloudformation/service.yaml \
		--s3-bucket ${S3_BUCKET_NAME} \
		--output-template-file build/service.yaml

cfn-service-deploy: cfn-service-package docker-push
	$(eval SVC_PARAMS := $(shell cat ./config/${ENVIRONMENT}/service.cfg))
	$(eval INF_PARAMS := $(shell cat ./config/${ENVIRONMENT}/infrastructure.cfg))
	aws cloudformation deploy \
	--template-file build/service.yaml \
	--stack-name ${SERVICE_STACK_NAME} \
	--capabilities CAPABILITY_NAMED_IAM \
	--parameter-overrides \
		${PROJECT_CFN_PARAMS} \
		${SVC_PARAMS} \
		${INF_PARAMS} \
		ContainerPort=${PORT} \
		ServiceEndpoints=${SERVICE_ENDPOINTS}

.PHONY: cfn-validate
cfn-validate:
	aws cloudformation validate-template --template-body "file://cloudformation/service-shared.yaml"
	aws cloudformation validate-template --template-body "file://cloudformation/infrastructure.yaml"
	aws cloudformation validate-template --template-body "file://cloudformation/service.yaml"

.DEFAULT_GOAL := help
