SERVICE_NAME ?= sbs-file-size-calculator
API_VERSION = v1
PORT = 8080

# Deployment settings
PROJECT_NAME ?= sbs-file-size-calculator
TEAM_NAME = sbs

## Deployment environment/stage
ENVIRONMENT ?= dev

## AWS configuration
AWS_ACCOUNT_ID = $(shell aws sts get-caller-identity --out json --query 'Account')
AWS_REGION = ap-southeast-2
