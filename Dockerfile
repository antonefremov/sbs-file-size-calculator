FROM golang:1.19-buster AS builder
RUN apt-get -y update && apt-get -y upgrade && apt-get install -y --no-install-recommends ffmpeg

# Set necessary environment variables needed for our image
ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

# Move to working directory /build
WORKDIR /build

# Copy the code into the container
COPY ./ .

# Build the application
RUN go build -o calcSvc

# Move to /dist directory as the place for resulting binary folder
WORKDIR /dist

# Copy binary from build to main folder
RUN cp /build/calcSvc .

EXPOSE 8080

# # Build a small image
# FROM scratch
# COPY --from=builder /dist/calcSvc /
# # RUN chmod +x ./calcSvc
CMD ["chmod +x /dist/calcSvc"]
ENTRYPOINT ["/dist/calcSvc"]
