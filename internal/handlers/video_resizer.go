package handlers

import (
	"errors"
	"fmt"
	"io"
	"os"

	"net/http"
	"net/url"
	"path/filepath"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/hlog"
	qrcode "github.com/skip2/go-qrcode"
	ffmpeg "github.com/u2takey/ffmpeg-go"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/router"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/services"
)

type VideoResizeOut struct {
	Size     int    `json:"size"`
	Category string `json:"category"`
}

func FromVideoResizeOut(out services.VideoResizeOut) VideoResizeOut {
	return VideoResizeOut{
		Size:     out.Size,
		Category: string(out.Category),
	}
}

// VideoResizeRouteCfg defines the interface that required for VideoResizeRoute
type VideoResizeRouteCfg interface{}

const (
	in     = "in.mp4"
	out    = "out.mp4"
	assets = "assets"
	videos = "videos"
	lbURL  = "http://exter-Publi-1VVHZ2X5GG216-652839367.ap-southeast-2.elb.amazonaws.com/v2/resizedVideo"
)

// VideoResizeRoute "/resize" route configuration
func VideoResizeRoute(cfg VideoResizeRouteCfg, srv services.VideoResizeService) router.Route {
	return router.Route{
		Path:   "/resize",
		Method: http.MethodPost,
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log := hlog.FromRequest(r)
			VideoResizeHandler(w, r, srv, log)
		}),
	}
}

// ServeResizedVideoRoute "/resizedVideo" route configuration
func ServeResizedVideoRoute() router.Route {
	return router.Route{
		Path:   "/resizedVideo",
		Method: http.MethodGet,
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			storageDirPath := filepath.Join(assets, videos)
			fmt.Println("!!! Storage dir path:", storageDirPath)
			outFileName := filepath.Join(storageDirPath, out)
			entries, err := os.ReadDir(storageDirPath)
			if err != nil {
				fmt.Printf("Error reading storage directory when requesting the /resizedVideo path: %s", err.Error())
			}
			for _, e := range entries {
				fmt.Printf(fmt.Sprintln("Storage dir entry on requesting the /resizedVideo path:", e.Name()))
			}

			w.Header().Set("Content-Type", "video/mp4")
			http.ServeFile(w, r, outFileName)
		}),
	}
}

// AssetsRoute "/assets" route configuration
func AssetsRoute() router.Route {
	return router.Route{
		Path:   "/",
		Method: http.MethodGet,
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "text/html; charset=utf-8")
			// w.WriteHeader(http.StatusOK)
			http.ServeFile(w, r, "./assets/index.html")
		}),
	}
}

// VideoResizeHandler handles the incoming request on the predefined route
func VideoResizeHandler(w http.ResponseWriter, r *http.Request, srv services.VideoResizeService, l *zerolog.Logger) {
	err := r.ParseMultipartForm(4 << 20) // limit to 4Mb
	if err != nil {
		l.Error().Msgf("Error parsing multipart form %s", err.Error())
	}
	storageDirPath := filepath.Join(assets, videos)
	err = os.MkdirAll(storageDirPath, 0755)
	if err != nil {
		l.Error().Msgf("Error creating the storage directory %s", err.Error())
	}

	files := r.MultipartForm.File["file"]
	for _, f := range files {
		file, err := f.Open()
		defer file.Close()
		if err != nil {
			l.Error().Msgf("Error opening file %s", err.Error())
		}
		l.Info().Msgf("File name: %s", f.Filename)

		inFileName := filepath.Join(storageDirPath, in)
		src, err := os.Create(inFileName)
		if err != nil {
			l.Error().Msgf("Error creating in file %s", err.Error())
		}
		defer src.Close()

		entries, err := os.ReadDir(storageDirPath)
		if err != nil {
			l.Error().Msgf("Error reading storage directory %s", err.Error())
		}
		for _, e := range entries {
			l.Info().Msg(fmt.Sprintln("Storage dir entry after creating an in file:", e.Name()))
		}

		_, err = io.Copy(src, file)

		outFileName := filepath.Join(storageDirPath, out)
		dst, err := os.Create(outFileName)
		if err != nil {
			l.Error().Msgf("Error creating out file %s", err.Error())
		}
		defer dst.Close()

		err = ffmpeg.Input(inFileName, ffmpeg.KwArgs{"ss": 1}).
			Output(outFileName, ffmpeg.KwArgs{"t": 2}).OverWriteOutput().Run() // cut the video to 2 seconds
		if err != nil {
			l.Error().Msgf("Error calling ffmpeg: %s", err.Error())
		}

		entries, err = os.ReadDir(storageDirPath)
		if err != nil {
			l.Error().Msgf("Error reading storage directory %s", err.Error())
		}
		for _, e := range entries {
			l.Info().Msg(fmt.Sprintln("Storage dir entry after creating an out file:", e.Name()))
		}

		var png []byte
		png, err = qrcode.Encode(lbURL, qrcode.Medium, 256)
		if err != nil {
			l.Error().Msg("Error generating qr-code")
		}
		w.Header().Set("Content-Type", "image/png")
		// w.WriteHeader(http.StatusOK)
		_, _ = w.Write(png)
	}
}

func toVideoResizeIn(inURL string) (services.VideoResizeIn, error) {
	if inURL == "" {
		return services.VideoResizeIn{}, errors.New("the 'file' query parameter must be provided")
	}

	url, err := url.ParseRequestURI(inURL)
	if err != nil {
		return services.VideoResizeIn{}, errors.New("the 'file' query parameter value must be a valid URL")
	}

	return services.VideoResizeIn{
		URL: url,
	}, nil
}
