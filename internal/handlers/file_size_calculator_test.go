package handlers_test

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/test-go/testify/assert"
	"github.com/test-go/testify/mock"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/errutils"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/model"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/handlers"
	hmocks "gitlab.com/antonefremov/sbs-file-size-calculator/internal/handlers/mocks"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/services"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/services/mocks"
)

//go:generate go run github.com/vektra/mockery/v2/ --name=FileSizeCalcRouteCfg --case=snake

func runCalcHandler(calcSvc services.FileSizeCalcService, w http.ResponseWriter, r *http.Request) {
	serveHTTP(handlers.FileSizeCalcRoute(&hmocks.FileSizeCalcRouteCfg{}, calcSvc), w, r)
}

func TestFileSizeCalculatorHandler(t *testing.T) {
	t.Run("succeeds when a valid URL is provided", func(t *testing.T) {
		// Given
		url, _ := url.Parse("https://www.sbs.com.au/small-image.jpeg")
		inSrvURL := services.FileSizeCalcIn{
			URL: url,
		}
		outSrvResponse := services.FileSizeCalcOut{
			Size:     1000,
			Category: model.FileSizeCategorySmall,
		}
		expectedHandlerResponseSB := new(strings.Builder)
		expectedHandlerResponseSB.WriteString(`{
			"size": 1000,
			"category": "SMALL"
		}`)

		r := httptest.NewRequest(http.MethodGet, "/calculator", nil)
		q := r.URL.Query()
		q.Add("file", "https://www.sbs.com.au/small-image.jpeg")

		r.URL.RawQuery = q.Encode()
		w := httptest.NewRecorder()

		calcSvc := &mocks.FileSizeCalcService{}
		calcSvc.On("Calculate", inSrvURL, mock.Anything).Return(outSrvResponse, nil).Once()

		// When
		runCalcHandler(calcSvc, w, r)

		// Then
		assert.Equal(t, http.StatusOK, w.Code)
		assert.JSONEq(t, expectedHandlerResponseSB.String(), w.Body.String())
		calcSvc.AssertExpectations(t)
	})

	t.Run("returns 'bad request' when the 'file' query param is missing", func(t *testing.T) {
		// Given
		r := httptest.NewRequest(http.MethodGet, "/calculator", nil)
		q := r.URL.Query()
		q.Add("", "https://www.sbs.com.au/small-image.jpeg")

		r.URL.RawQuery = q.Encode()
		w := httptest.NewRecorder()

		calcSvc := &mocks.FileSizeCalcService{}

		// When
		runCalcHandler(calcSvc, w, r)

		// Then
		assert.Equal(t, http.StatusBadRequest, w.Code)
		assertErrorCode(t, handlers.RequestValidationErrorCode, w)
		calcSvc.AssertExpectations(t)
	})

	t.Run("returns 'bad request' when the 'file' query param is malformed", func(t *testing.T) {
		// Given
		r := httptest.NewRequest(http.MethodGet, "/calculator", nil)
		q := r.URL.Query()
		q.Add("unknown", "https://www.sbs.com.au/small-image.jpeg")

		r.URL.RawQuery = q.Encode()
		w := httptest.NewRecorder()

		calcSvc := &mocks.FileSizeCalcService{}

		// When
		runCalcHandler(calcSvc, w, r)

		// Then
		assert.Equal(t, http.StatusBadRequest, w.Code)
		assertErrorCode(t, handlers.RequestValidationErrorCode, w)
		calcSvc.AssertExpectations(t)
	})

	t.Run("returns 'bad request' when the 'file' query param value is not a URL", func(t *testing.T) {
		// Given
		r := httptest.NewRequest(http.MethodGet, "/calculator", nil)
		q := r.URL.Query()
		q.Add("file", "golang.org")

		r.URL.RawQuery = q.Encode()
		w := httptest.NewRecorder()

		calcSvc := &mocks.FileSizeCalcService{}

		// When
		runCalcHandler(calcSvc, w, r)

		// Then
		assert.Equal(t, http.StatusBadRequest, w.Code)
		assertErrorCode(t, handlers.RequestValidationErrorCode, w)
		calcSvc.AssertExpectations(t)
	})

	t.Run("returns 'server error' when a service error occurs", func(t *testing.T) {
		// Given
		url, _ := url.Parse("https://www.sbs.com.au/small-image.jpeg")
		inSrvURL := services.FileSizeCalcIn{
			URL: url,
		}

		r := httptest.NewRequest(http.MethodGet, "/calculator", nil)
		q := r.URL.Query()
		q.Add("file", "https://www.sbs.com.au/small-image.jpeg")

		r.URL.RawQuery = q.Encode()
		w := httptest.NewRecorder()

		calcSvc := &mocks.FileSizeCalcService{}
		calcSvc.On("Calculate", inSrvURL, mock.Anything).Return(services.FileSizeCalcOut{}, errors.New("service error")).Once()

		// When
		runCalcHandler(calcSvc, w, r)

		// Then
		assert.Equal(t, http.StatusInternalServerError, w.Code)
		assertErrorCode(t, handlers.ServiceErrorCode, w)
		calcSvc.AssertExpectations(t)
	})

	t.Run("returns 'server error' when a service error occurs", func(t *testing.T) {
		// Given
		url, _ := url.Parse("https://www.sbs.com.au/small-image.jpeg")
		inSrvURL := services.FileSizeCalcIn{
			URL: url,
		}
		deErr := errutils.DownstreamError{
			Msg:     "Downstream error",
			Err:     fmt.Errorf("response status was not successful: %d", 500),
			DestURL: "https://host-with-an-error.com",
		}

		r := httptest.NewRequest(http.MethodGet, "/calculator", nil)
		q := r.URL.Query()
		q.Add("file", "https://www.sbs.com.au/small-image.jpeg")

		r.URL.RawQuery = q.Encode()
		w := httptest.NewRecorder()

		calcSvc := &mocks.FileSizeCalcService{}
		calcSvc.On("Calculate", inSrvURL, mock.Anything).Return(services.FileSizeCalcOut{}, &deErr).Once()

		// When
		runCalcHandler(calcSvc, w, r)

		// Then
		assert.Equal(t, http.StatusFailedDependency, w.Code)
		calcSvc.AssertExpectations(t)
	})
}
