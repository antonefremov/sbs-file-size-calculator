package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/rs/zerolog"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/errutils"
)

const (
	// RequestParserErrorCode is the error code returned when request parser has failed
	RequestParserErrorCode = "RequestParserError"
	// RequestValidationErrorCode is the error code returned when request validation has failed
	RequestValidationErrorCode = "RequestValidationError"
	// ResponseSerializationErrorCode is the error code returned when response serialization has failed
	ResponseSerializationErrorCode = "ResponseSerializationError"
	// ServiceErrorCode is the error code returned when service has failed
	ServiceErrorCode = "ServiceError"
	// AuthenticationErrorCode is the error code returned when authentication has failed
	AuthenticationErrorCode = "AuthenticationError"
	// RequestNotFoundErrorCode is the error code returned when a user was not found
	RequestNotFoundErrorCode = "UserNotFound"
)

type ErrorPayload struct {
	Code        string `json:"code"`
	Description string `json:"description"`
}

func getErrorPayload(err error) ([]byte, int, error) {
	pe := errutils.GetPublicError(err)
	if pe == nil {
		pe = errutils.NewPublicError(http.StatusInternalServerError, "UnknownInternalError", "An unexpected error occurred").(errutils.PublicError)
	}

	payload, err := json.Marshal(ErrorPayload{
		Code:        pe.Code(),
		Description: pe.Description(),
	})

	return payload, pe.HTTPStatus(), err
}

func writeErrorResponse(w http.ResponseWriter, logger *zerolog.Logger, err error) {
	logger.Err(err).Msg("handler error response")

	payload, status, err := getErrorPayload(err)
	if err != nil {
		http.Error(w, "Failed to encode error", http.StatusInternalServerError)
	} else {
		http.Error(w, string(payload), status)
	}
}

func wrapResponseSerializationError(err error) error {
	return errutils.WrapPublicError(err, http.StatusInternalServerError, ResponseSerializationErrorCode, "Failed to format response data")
}

func wrapRequestValidationError(err error) error {
	return errutils.WrapPublicError(err, http.StatusBadRequest, RequestValidationErrorCode, err.Error())
}

func writeDownstreamErrorResponse(w http.ResponseWriter, logger *zerolog.Logger, err error) {
	logger.Err(err).Msg("downstream service error response")

	if err != nil {
		http.Error(w, err.Error(), http.StatusFailedDependency)
	}
}

func wrapServiceError(err error) error {
	// a service can explicitly return a public error
	if errutils.HasPublicError(err) {
		return err
	}

	// or rely on the following default handling
	return errutils.WrapPublicError(err, http.StatusInternalServerError, ServiceErrorCode, "Internal Service Error occurred")
}
