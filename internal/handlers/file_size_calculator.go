package handlers

import (
	"encoding/json"
	"errors"

	"net/http"
	"net/url"

	pkgerr "github.com/pkg/errors"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/hlog"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/errutils"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/router"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/services"
)

type FileSizeCalcOut struct {
	Size     int    `json:"size"`
	Category string `json:"category"`
}

func FromFileSizeCalcOut(out services.FileSizeCalcOut) FileSizeCalcOut {
	return FileSizeCalcOut{
		Size:     out.Size,
		Category: string(out.Category),
	}
}

// FileSizeCalcRouteCfg defines the interface that required for FileSizeCalcRoute
type FileSizeCalcRouteCfg interface{}

// FileSizeCalcRoute "/calculator" route configuration
func FileSizeCalcRoute(cfg FileSizeCalcRouteCfg, srv services.FileSizeCalcService) router.Route {
	return router.Route{
		Path:   "/calculator",
		Method: http.MethodGet,
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log := hlog.FromRequest(r)
			FileSizeCalcHandler(w, r, srv, log)
		}),
	}
}

// FileSizeCalcHandler handles the incoming request on the predefined route
func FileSizeCalcHandler(w http.ResponseWriter, r *http.Request, srv services.FileSizeCalcService, l *zerolog.Logger) {
	fileURL := r.URL.Query().Get("file")
	fileSizeCalcIn, err := toFileSizeCalcIn(fileURL)
	if err != nil {
		writeErrorResponse(w, l, wrapRequestValidationError(err))
		return
	}

	fileSizeCalcOut, err := srv.Calculate(fileSizeCalcIn, l)
	if err != nil {
		switch err := pkgerr.Cause(err).(type) {
		case *errutils.DownstreamError:
			writeDownstreamErrorResponse(w, l, err)
		default:
			writeErrorResponse(w, l, wrapServiceError(err))
		}
		return
	}

	body, err := json.Marshal(FromFileSizeCalcOut(fileSizeCalcOut))
	if err != nil {
		writeErrorResponse(w, l, wrapResponseSerializationError(err))
		return
	}

	w.Header().Set("Cache-Control", fileSizeCalcOut.MaxAge)
	w.WriteHeader(http.StatusOK)
	bytes, err := w.Write(body)
	if err != nil {
		l.Err(err).Send()
	}

	if bytes != len(body) {
		l.Warn().Msgf("wrote %d bytes but expected %d", bytes, len(body))
	}
}

func toFileSizeCalcIn(inURL string) (services.FileSizeCalcIn, error) {
	if inURL == "" {
		return services.FileSizeCalcIn{}, errors.New("the 'file' query parameter must be provided")
	}

	url, err := url.ParseRequestURI(inURL)
	if err != nil {
		return services.FileSizeCalcIn{}, errors.New("the 'file' query parameter value must be a valid URL")
	}

	return services.FileSizeCalcIn{
		URL: url,
	}, nil
}
