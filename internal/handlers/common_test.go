package handlers_test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/router"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/handlers"
)

func assertErrorCode(t *testing.T, expectedCode string, response *httptest.ResponseRecorder) {
	decoder := json.NewDecoder(response.Body)
	decoder.DisallowUnknownFields()

	var error handlers.ErrorPayload
	err := decoder.Decode(&error)
	require.NoError(t, err)
	assert.Equal(t, expectedCode, error.Code)
}

func serveHTTP(route router.Route, w http.ResponseWriter, r *http.Request) {
	router := mux.NewRouter()
	router.Handle(route.Path, route.Handler).Methods(route.Method)
	router.ServeHTTP(w, r)
}
