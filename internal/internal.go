package internal

import (
	"fmt"

	"github.com/rs/zerolog"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/router"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/server"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/config"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/handlers"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/services"
)

func SetupServer(logger *zerolog.Logger, cfg config.Config) *server.Server {
	basePath := fmt.Sprintf("/%v", cfg.APIVersion())
	corsSettings := cfg.CORSSettings()

	logger.Info().
		Str("basePath", basePath).
		Interface("corsSettings", corsSettings).
		Msg("creating service")

	r := router.New(router.WithCORSSettings(corsSettings))
	r.PathRouter(basePath).WithRoutes(
		handlers.FileSizeCalcRoute(cfg, services.NewFileSizeCalcService()),
		handlers.VideoResizeRoute(cfg, services.NewVideoResizeService()),
		handlers.ServeResizedVideoRoute(),
		handlers.AssetsRoute(),
	)

	s := server.New(logger,
		server.WithRouter(r),
		server.WithName(cfg.ServiceName()),
		server.WithHTTPAddress(cfg.HTTPAddress()),
		server.WithHTTPShutdownTimeout(cfg.HTTPShutdownTimeout()),
		server.WithHTTPReadTimeout(cfg.HTTPReadTimeout()),
		server.WithHTTPWriteTimeout(cfg.HTTPWriteTimeout()),
		server.WithHTTPIdleTimeout(cfg.HTTPIdleTimeout()),
		server.WithHTTPReadHeaderTimeout(cfg.HTTPReadHeaderTimeout()),
	)
	return s
}
