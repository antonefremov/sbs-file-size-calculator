package router

import (
	"encoding/json"
	"net/http"
)

const HealthPath string = "/health"
const healthStatusOK string = "OK"

type healthStatus struct {
	Status string `json:"status"`
}

func healthHandler(w http.ResponseWriter, r *http.Request) {
	status := healthStatus{
		Status: healthStatusOK,
	}

	body, err := json.Marshal(status)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	_, _ = w.Write(body)
}
