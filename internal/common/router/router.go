package router

import (
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/hlog"
	"github.com/rs/zerolog/log"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/authenticator"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/middlewares"
)

var defaultMiddlewares = []func(http.Handler) http.Handler{
	middlewares.PanicRecoveryHandler,
	middlewares.ResponseHeadersHandler,
}

type RouteSettings struct {
	AllowedSources []middlewares.RequestSource
	SkipAuth       bool // If SkipAuth = true, requests from all sources are accepted and will not be validated
}

func (u *RouteSettings) toAuthSettings(s *AuthenticatorSettings) *middlewares.AuthSettings {
	return &middlewares.AuthSettings{
		AllowedSources:     u.AllowedSources,
		SkipAuth:           u.SkipAuth,
		AudienceIdentifier: s.AudienceIdentifier,
	}
}

// Route define structure that handle request for route
type Route struct {
	Path          string
	Method        string
	RouteSettings RouteSettings
	Handler       http.Handler
}

type routerOptions struct {
	corsSettings  *CORSSettings
	authSettings  *AuthenticatorSettings
	healthHandler http.Handler
}

//TODO: For now we want to optionally hook up the auth middleware to each service, we can add default auth settings to make
//      auth middleware default
var defaultRouterOptions = routerOptions{
	healthHandler: http.HandlerFunc(healthHandler),
}

type Router struct {
	opts   routerOptions
	router *mux.Router
}

func (r *Router) WithMiddleware(mws ...mux.MiddlewareFunc) *Router {
	r.router.Use(mws...)
	return r
}

func (r *Router) WithPath(path string, cb func(*Router)) *Router {
	cb(r.PathRouter(path))
	return r
}

func (r *Router) WithHandler(path string, h http.Handler, s RouteSettings, methods ...string) *Router {
	h = r.withMiddlewares(path, h, s)

	route := r.router.Handle(path, h)
	if len(methods) > 0 {
		if r.opts.corsSettings != nil {
			methods = append(methods, http.MethodOptions)
		}
		route.Methods(methods...)
	}

	return r
}

func (r *Router) WithRoute(rr Route) *Router {
	return r.WithHandler(rr.Path, rr.Handler, rr.RouteSettings, rr.Method)
}

func (r *Router) WithRoutes(routes ...Route) *Router {
	for _, route := range routes {
		r.WithRoute(route)
	}
	return r
}

func (r *Router) PathRouter(p string) *Router {
	return &Router{
		router: r.router.PathPrefix(p).Subrouter(),
		opts:   r.opts,
	}
}

func (r *Router) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	r.router.ServeHTTP(rw, req)
}

func (r *Router) withMiddlewares(path string, h http.Handler, s RouteSettings) http.Handler {
	// This list of middlewares is processed in order from the bottom up
	h = middlewares.WithMiddlewares(h, middlewares.ResponseHeadersHandler)

	h = r.withAuthMiddleWare(h, s)

	// CORS should be processed before auth as the preflight check does not have credentials
	h = r.withCORSMiddleware(h)

	h = middlewares.WithMiddlewares(h, middlewares.RequestLogHandler)

	// Apply panic recovery middleware as soon as possible to provide best protection with
	// clear log message
	h = middlewares.WithMiddlewares(h, middlewares.PanicRecoveryHandler)

	// Middlewares that add essential info to log and request
	h = middlewares.WithMiddlewares(h, middlewares.CorrelationIDHandler)
	h = middlewares.WithMiddlewares(h, middlewares.RequestAttributesLogHandlers(path))

	// The hlog.NewHandler has to be processed at the first place,
	// inside the method a sub logger is created based on the zerolog
	// global logger and associated with request's context
	h = middlewares.WithMiddlewares(h, hlog.NewHandler(log.Logger))
	return h
}

func (r *Router) withCORSMiddleware(h http.Handler) http.Handler {
	if r.opts.corsSettings != nil {
		h = middlewares.WithMiddlewares(h, middlewares.CORSHandler(
			handlers.AllowedOrigins(r.opts.corsSettings.AllowedOrigins),
			handlers.AllowedHeaders(r.opts.corsSettings.AllowedHeaders),
			handlers.AllowedMethods(r.opts.corsSettings.AllowedMethods),
		))
	}
	return h
}

func (r *Router) withAuthMiddleWare(h http.Handler, s RouteSettings) http.Handler {
	if r.opts.authSettings != nil {
		a := authenticator.New(r.opts.authSettings.JWKSEndpoint)
		h = middlewares.WithMiddlewares(h, middlewares.AuthHandler(
			a,
			s.toAuthSettings(r.opts.authSettings)))
	}

	return h
}

func New(opt ...Option) *Router {
	r := mux.NewRouter()
	r.NotFoundHandler = requestMismatchHandler(http.StatusNotFound)
	r.MethodNotAllowedHandler = requestMismatchHandler(http.StatusMethodNotAllowed)

	opts := defaultRouterOptions
	for _, o := range opt {
		o.apply(&opts)
	}

	h := middlewares.WithMiddlewares(
		opts.healthHandler,
		defaultMiddlewares...,
	)
	r.Handle(HealthPath, h)

	return &Router{router: r, opts: opts}
}

func requestMismatchHandler(statusCode int) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Logger.Info().
			Str("uri", r.URL.Path).
			Str("method", r.Method).
			Str("userAgent", r.Header.Get("User-Agent")).
			Str("referer", r.Header.Get("Referer")).
			Str("remoteAddr", r.RemoteAddr).
			Str("correlationID", r.Header.Get(middlewares.CorrelationIDHeader)).
			Int("statusCode", statusCode).
			Msg("receive a request that doesn't match any registered route")

		w.WriteHeader(statusCode)
	})
}

type Option interface {
	apply(*routerOptions)
}

type funcOption struct {
	f func(*routerOptions)
}

func (fo *funcOption) apply(o *routerOptions) {
	fo.f(o)
}

func newFuncOption(f func(*routerOptions)) *funcOption {
	return &funcOption{f}
}

func WithCORSSettings(s CORSSettings) Option {
	return newFuncOption(func(o *routerOptions) {
		o.corsSettings = &s
	})
}

func WithAuthenticatorSettings(s AuthenticatorSettings) Option {
	return newFuncOption(func(o *routerOptions) {
		o.authSettings = &s
	})
}

func WithHealthHandler(h http.Handler) Option {
	return newFuncOption(func(o *routerOptions) {
		o.healthHandler = h
	})
}

type CORSSettings struct {
	AllowedOrigins []string
	AllowedHeaders []string
	AllowedMethods []string
}

type AuthenticatorSettings struct {
	JWKSEndpoint       string
	AudienceIdentifier string
}
