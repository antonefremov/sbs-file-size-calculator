package router_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/google/uuid"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/middlewares"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/router"
)

var customHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, `{"message": "customHandler"}`)
})

var customRoute = router.Route{
	Path:   "/custom",
	Method: http.MethodPut,
	RouteSettings: router.RouteSettings{
		AllowedSources: []middlewares.RequestSource{
			middlewares.RequestSourceService,
			middlewares.RequestSourceUser,
		},
		SkipAuth: false,
	},
	Handler: customHandler,
}

var panicHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	panic("")
})

func TestDefaultRouter(t *testing.T) {
	t.Run("default router has a health endpoint", func(t *testing.T) {
		req := httptest.NewRequest(http.MethodGet, router.HealthPath, nil)
		rr := httptest.NewRecorder()

		router.New().ServeHTTP(rr, req)

		res := rr.Result()
		defer res.Body.Close()
		body, _ := ioutil.ReadAll(res.Body)

		assert.Equal(t, http.StatusOK, res.StatusCode)
		assert.JSONEq(t, `{"status": "OK"}`, string(body))
		assert.Equal(t, "application/json", res.Header.Get("Content-Type"))
	})

	t.Run("health endpoint does not have log middleware", func(t *testing.T) {
		req := httptest.NewRequest(http.MethodGet, router.HealthPath, nil)
		rr := httptest.NewRecorder()

		logOutput := &bytes.Buffer{}
		log.Logger = zerolog.New(logOutput)

		router.New().ServeHTTP(rr, req)
		assert.Empty(t, logOutput)
	})

	t.Run("default router sets panic recovery middleware to all routes", func(t *testing.T) {
		path := "/panic"
		req := httptest.NewRequest(http.MethodGet, path, nil)
		rr := httptest.NewRecorder()

		router.New().
			WithHandler(path, panicHandler, router.RouteSettings{}).
			ServeHTTP(rr, req)

		res := rr.Result()
		defer res.Body.Close()

		assert.Equal(t, http.StatusInternalServerError, res.StatusCode)
	})

	t.Run("default router sets response headers middleware to all routes", func(t *testing.T) {
		path := "/custom"
		req := httptest.NewRequest(http.MethodGet, path, nil)
		rr := httptest.NewRecorder()

		router.New().
			WithHandler(path, customHandler, router.RouteSettings{}).
			ServeHTTP(rr, req)

		res := rr.Result()
		defer res.Body.Close()
		body, _ := ioutil.ReadAll(res.Body)

		assert.Equal(t, http.StatusOK, res.StatusCode)
		assert.JSONEq(t, `{"message": "customHandler"}`, string(body))
		assert.Equal(t, "application/json", res.Header.Get("Content-Type"))
	})

	t.Run("default router sets correlation id middleware for custom routes", func(t *testing.T) {
		router := router.New().WithRoute(customRoute)

		req := httptest.NewRequest(http.MethodPut, customRoute.Path, nil)
		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		res := rr.Result()
		defer res.Body.Close()

		assert.Equal(t, http.StatusOK, res.StatusCode)
		assert.NotEmpty(t, res.Header.Get("X-Correlation-Id"))
	})

	t.Run("default router sets logging middleware for custom routes", func(t *testing.T) {
		path := "/custom"
		req := httptest.NewRequest(http.MethodGet, path, nil)
		rr := httptest.NewRecorder()

		logOutput := &bytes.Buffer{}
		log.Logger = zerolog.New(logOutput)

		router.New().
			WithHandler(path, customHandler, router.RouteSettings{}).
			ServeHTTP(rr, req)

		assert.NotEmpty(t, logOutput)
	})

	t.Run("default router sets CORS middleware for custom routes", func(t *testing.T) {
		corsSettings := router.CORSSettings{
			AllowedOrigins: []string{"https://foo.bar.org"},
			AllowedHeaders: []string{"Content-Type", "X-Correlation-Id"},
			AllowedMethods: []string{
				http.MethodDelete, http.MethodGet, http.MethodHead,
				http.MethodPatch, http.MethodPost, http.MethodPut,
				http.MethodOptions,
			},
		}

		router := router.New(router.WithCORSSettings(corsSettings)).
			WithRoute(customRoute)

		t.Run("correctly serves request with OPTION HTTP method", func(t *testing.T) {
			req := httptest.NewRequest(http.MethodOptions, customRoute.Path, nil)
			rr := httptest.NewRecorder()

			req.Header.Set("Origin", "https://foo.bar.org")
			req.Header.Set("Access-Control-Request-Headers", "Content-Type")
			req.Header.Set("Access-Control-Request-Method", "PUT")

			router.ServeHTTP(rr, req)

			res := rr.Result()
			defer res.Body.Close()

			assert.Equal(t, http.StatusOK, res.StatusCode)
			assert.Equal(t, "https://foo.bar.org", res.Header.Get("Access-Control-Allow-Origin"))
			assert.Equal(t, "PUT", res.Header.Get("Access-Control-Allow-Methods"))
		})

		t.Run("correctly serves PUT request", func(t *testing.T) {
			req := httptest.NewRequest(http.MethodPut, customRoute.Path, nil)
			rr := httptest.NewRecorder()

			req.Header.Set("Origin", "https://foo.bar.org")

			router.ServeHTTP(rr, req)

			res := rr.Result()
			defer res.Body.Close()
			body, _ := ioutil.ReadAll(res.Body)

			assert.Equal(t, http.StatusOK, res.StatusCode)
			assert.Equal(t, "https://foo.bar.org", res.Header.Get("Access-Control-Allow-Origin"))
			assert.Equal(t, `{"message": "customHandler"}`, string(body))
		})
	})

	t.Run("default router sets Auth middleware for custom routes", func(t *testing.T) {
		s := router.AuthenticatorSettings{
			JWKSEndpoint:       "https://ttcloud/.well-known/jwks.json",
			AudienceIdentifier: "https://tt-v-appli-q57zfx43lgat-1922719955.ap-southeast-2.elb.amazonaws.com/v1/",
		}

		router := router.New(router.WithAuthenticatorSettings(s)).
			WithRoute(customRoute)

		req := httptest.NewRequest(http.MethodPut, customRoute.Path, nil)
		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)

		res := rr.Result()
		defer res.Body.Close()

		assert.Equal(t, http.StatusUnauthorized, res.StatusCode)
	})

	t.Run("custom route explicitly sets allowed HTTP method", func(t *testing.T) {
		router := router.New().WithRoute(customRoute)

		t.Run("serves request with allowed HTTP method", func(t *testing.T) {
			req := httptest.NewRequest(http.MethodPut, customRoute.Path, nil)
			rr := httptest.NewRecorder()

			router.ServeHTTP(rr, req)

			res := rr.Result()
			defer res.Body.Close()

			assert.Equal(t, http.StatusOK, res.StatusCode)
		})

		t.Run("rejects request with not allowed HTTP method", func(t *testing.T) {
			req := httptest.NewRequest(http.MethodPost, customRoute.Path, nil)
			rr := httptest.NewRecorder()

			router.ServeHTTP(rr, req)

			res := rr.Result()
			defer res.Body.Close()

			assert.Equal(t, http.StatusMethodNotAllowed, res.StatusCode)
		})
	})

	t.Run("router registers multiple routes", func(t *testing.T) {
		rA := router.Route{
			Path:   "/a",
			Method: http.MethodGet,
			Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				fmt.Fprint(w, "A")
			}),
		}

		rB := router.Route{
			Path:   "/b",
			Method: http.MethodGet,
			Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				fmt.Fprint(w, "B")
			}),
		}

		router := router.New().WithRoutes(rA, rB)
		testCases := []struct {
			path string
			body string
		}{
			{
				path: rA.Path,
				body: "A",
			},
			{
				path: rB.Path,
				body: "B",
			},
		}

		for _, tC := range testCases {
			req := httptest.NewRequest(http.MethodGet, tC.path, nil)
			rr := httptest.NewRecorder()

			router.ServeHTTP(rr, req)

			res := rr.Result()
			defer res.Body.Close()
			body, _ := ioutil.ReadAll(res.Body)

			assert.Equal(t, http.StatusOK, res.StatusCode)
			assert.Equal(t, tC.body, string(body))
		}
	})

	t.Run("router registers a routes to a path", func(t *testing.T) {
		v1Path := "/v1/base"
		v2Path := "/v2/base"
		routePath := "/a"
		v1R := router.Route{
			Path:   routePath,
			Method: http.MethodGet,
			Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				fmt.Fprint(w, "a")
			}),
		}

		v2R := router.Route{
			Path:   routePath,
			Method: http.MethodGet,
			Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				fmt.Fprint(w, "A")
			}),
		}

		r := router.New()
		r.PathRouter(v1Path).WithRoute(v1R)
		r.WithRoute(customRoute)
		r.PathRouter(v2Path).WithRoute(v2R)

		t.Run("serves request to health route", func(t *testing.T) {
			req := httptest.NewRequest(http.MethodGet, router.HealthPath, nil)
			rr := httptest.NewRecorder()

			r.ServeHTTP(rr, req)

			res := rr.Result()
			defer res.Body.Close()

			assert.Equal(t, http.StatusOK, res.StatusCode)
		})

		t.Run("serves request to registered custom route", func(t *testing.T) {
			// make sure it's not overwritten
			req := httptest.NewRequest(http.MethodPut, customRoute.Path, nil)
			rr := httptest.NewRecorder()

			r.ServeHTTP(rr, req)

			res := rr.Result()
			defer res.Body.Close()

			assert.Equal(t, http.StatusOK, res.StatusCode)
		})

		t.Run("serves request to registered v1 route", func(t *testing.T) {
			req := httptest.NewRequest(http.MethodGet, v1Path+routePath, nil)
			rr := httptest.NewRecorder()

			r.ServeHTTP(rr, req)

			res := rr.Result()
			defer res.Body.Close()
			body, _ := ioutil.ReadAll(res.Body)

			assert.Equal(t, http.StatusOK, res.StatusCode)
			assert.Equal(t, "a", string(body))
		})

		t.Run("serves request to registered v1 route", func(t *testing.T) {
			req := httptest.NewRequest(http.MethodGet, v2Path+routePath, nil)
			rr := httptest.NewRecorder()

			r.ServeHTTP(rr, req)

			res := rr.Result()
			defer res.Body.Close()
			body, _ := ioutil.ReadAll(res.Body)

			assert.Equal(t, http.StatusOK, res.StatusCode)
			assert.Equal(t, "A", string(body))
		})

		t.Run("reject requests to unregistered route", func(t *testing.T) {
			req := httptest.NewRequest(http.MethodGet, "/abc", nil)
			rr := httptest.NewRecorder()

			r.ServeHTTP(rr, req)

			res := rr.Result()
			defer res.Body.Close()

			assert.Equal(t, http.StatusNotFound, res.StatusCode)
		})
	})

	t.Run("router logs requests that don't match any registered route", func(t *testing.T) {
		basePath := "/v1/base"
		routePath := "/a"
		route := router.Route{
			Path:   routePath,
			Method: http.MethodGet,
			Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				fmt.Fprint(w, "a")
			}),
		}

		r := router.New()
		r.PathRouter(basePath).WithRoute(route)
		r.WithRoute(customRoute)

		type logOutput struct {
			Level         string `json:"level"`
			URI           string `json:"uri"`
			Method        string `json:"method"`
			Agent         string `json:"userAgent"`
			Referer       string `json:"referer"`
			Type          string `json:"mismatchType"`
			Status        int    `json:"statusCode"`
			Message       string `json:"message"`
			CorrelationID string `json:"correlationID"`
		}

		t.Run("router logs requests to unregistered path", func(t *testing.T) {
			out := &bytes.Buffer{}
			log.Logger = zerolog.New(out)

			correlationID := uuid.New()

			req := httptest.NewRequest(http.MethodGet, "/abc", nil)
			req.Header.Add("User-Agent", "test-user-agent")
			req.Header.Add("Referer", "test-referer")
			req.Header.Add(middlewares.CorrelationIDHeader, correlationID.String())

			rr := httptest.NewRecorder()

			r.ServeHTTP(rr, req)

			res := rr.Result()
			defer res.Body.Close()

			assert.Equal(t, http.StatusNotFound, res.StatusCode)

			logStruct := &logOutput{}
			err := json.Unmarshal(out.Bytes(), logStruct)
			assert.NoError(t, err)

			assert.Equal(t, "info", logStruct.Level)
			assert.Equal(t, "/abc", logStruct.URI)
			assert.Equal(t, http.MethodGet, logStruct.Method)
			assert.Equal(t, "test-user-agent", logStruct.Agent)
			assert.Equal(t, "test-referer", logStruct.Referer)
			assert.Equal(t, http.StatusNotFound, logStruct.Status)
			assert.Equal(t, "receive a request that doesn't match any registered route", logStruct.Message)
			assert.Equal(t, correlationID.String(), logStruct.CorrelationID)
		})

		t.Run("router logs requests with not allowed method", func(t *testing.T) {
			out := &bytes.Buffer{}
			log.Logger = zerolog.New(out)

			req := httptest.NewRequest(http.MethodPost, customRoute.Path, nil)
			req.Header.Add("User-Agent", "test-user-agent")
			req.Header.Add("Referer", "test-referer")

			rr := httptest.NewRecorder()

			r.ServeHTTP(rr, req)

			res := rr.Result()
			defer res.Body.Close()

			assert.Equal(t, http.StatusMethodNotAllowed, res.StatusCode)

			logStruct := &logOutput{}
			err := json.Unmarshal(out.Bytes(), logStruct)
			assert.NoError(t, err)

			assert.Equal(t, "info", logStruct.Level)
			assert.Equal(t, customRoute.Path, logStruct.URI)
			assert.Equal(t, http.MethodPost, logStruct.Method)
			assert.Equal(t, "test-user-agent", logStruct.Agent)
			assert.Equal(t, "test-referer", logStruct.Referer)
			assert.Equal(t, http.StatusMethodNotAllowed, logStruct.Status)
			assert.Equal(t, "receive a request that doesn't match any registered route", logStruct.Message)
			assert.Equal(t, "", logStruct.CorrelationID)
		})
	})
}
