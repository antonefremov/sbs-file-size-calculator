package auth

import (
	"encoding/json"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
)

type Claims struct {
	// According to https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.3, there could be either a single
	//  audience string or an array of audience strings in the JWT, so we use our customized Audience field.
	Audience []string
	UserID   uuid.UUID
	jwt.StandardClaims
}

// the struct used for customized json marshaling and unmarshaling method
type rawClaims struct {
	Audience interface{} `json:"aud,omitempty"`
	UserID   uuid.UUID   `json:"https://blackmagicdesign.com/userId"`
	jwt.StandardClaims
}

func (c Claims) MarshalJSON() ([]byte, error) {
	r := rawClaims{
		UserID:         c.UserID,
		StandardClaims: c.StandardClaims,
	}

	if len(c.Audience) == 1 {
		r.Audience = c.Audience[0]
	}

	if len(c.Audience) > 1 {
		r.Audience = c.Audience
	}

	return json.Marshal(r)
}

func (c *Claims) UnmarshalJSON(data []byte) error {
	r := rawClaims{}
	err := json.Unmarshal(data, &r)
	if err != nil {
		return err
	}

	c.UserID = r.UserID
	c.StandardClaims = r.StandardClaims

	if r.Audience != nil {
		if audiences, ok := r.Audience.([]interface{}); ok {
			for _, aud := range audiences {
				c.Audience = append(c.Audience, aud.(string))
			}
		} else {
			c.Audience = append(c.Audience, r.Audience.(string))
		}
	}

	return nil
}

func (c Claims) Valid() error {
	err := c.StandardClaims.Valid()
	if err != nil {
		return err
	}

	checkNilUUID := func(value interface{}) error {
		s, _ := value.(uuid.UUID)

		if s == uuid.Nil {
			return jwt.NewValidationError("UserID is invalid", jwt.ValidationErrorClaimsInvalid)
		}

		return nil
	}

	return validation.Validate(c.UserID, validation.Required, validation.By(checkNilUUID))
}
