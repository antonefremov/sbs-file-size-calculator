package auth_test

import (
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/auth"
)

func TestClaims(t *testing.T) {
	t.Run("successfully marshaled with one audience", func(t *testing.T) {
		userID := "72f19d99-4ab8-498e-9355-2b9f9c1f04c3"
		claims := auth.Claims{
			UserID:   uuid.MustParse(userID),
			Audience: []string{"https://ttdev.au.auth0.com/userinfo"},
		}

		b, err := json.Marshal(&claims)
		require.NoError(t, err)

		expected := `{
			"https://blackmagicdesign.com/userId": "72f19d99-4ab8-498e-9355-2b9f9c1f04c3",
			"aud": "https://ttdev.au.auth0.com/userinfo"
		}`
		assert.JSONEq(t, expected, string(b))
	})

	t.Run("successfully marshaled with multiple audiences", func(t *testing.T) {
		userID := "72f19d99-4ab8-498e-9355-2b9f9c1f04c3"
		claims := auth.Claims{
			UserID: uuid.MustParse(userID),
			Audience: []string{
				"https://ttdev.au.auth0.com/userinfo1",
				"https://ttdev.au.auth0.com/userinfo2",
			},
		}

		b, err := json.Marshal(&claims)
		require.NoError(t, err)

		expected := `{
			"https://blackmagicdesign.com/userId": "72f19d99-4ab8-498e-9355-2b9f9c1f04c3",
			"aud": [
				"https://ttdev.au.auth0.com/userinfo1",
				"https://ttdev.au.auth0.com/userinfo2"
				]
		}`
		assert.JSONEq(t, expected, string(b))
	})

	t.Run("successfully unmarshal when there is an array of audience", func(t *testing.T) {
		input := `{
			"https://blackmagicdesign.com/userId": "72f19d99-4ab8-498e-9355-2b9f9c1f04c3",
			"aud": [
				"https://ttdev.au.auth0.com/userinfo1",
				"https://ttdev.au.auth0.com/userinfo2"
				]
		}`

		claims := auth.Claims{}
		err := json.Unmarshal([]byte(input), &claims)
		require.NoError(t, err)

		assert.Equal(t, uuid.MustParse("72f19d99-4ab8-498e-9355-2b9f9c1f04c3"), claims.UserID)

		assert.ElementsMatch(t,
			[]string{
				"https://ttdev.au.auth0.com/userinfo1",
				"https://ttdev.au.auth0.com/userinfo2",
			},
			claims.Audience,
		)

		assert.NoError(t, claims.Valid())
	})

	t.Run("successfully unmarshal when there is one audience", func(t *testing.T) {
		input := `{
			"https://blackmagicdesign.com/userId": "72f19d99-4ab8-498e-9355-2b9f9c1f04c3",
			"aud":"https://ttdev.au.auth0.com/userinfo1"
		}`

		claims := auth.Claims{}
		err := json.Unmarshal([]byte(input), &claims)
		require.NoError(t, err)

		assert.Equal(t, uuid.MustParse("72f19d99-4ab8-498e-9355-2b9f9c1f04c3"), claims.UserID)
		assert.ElementsMatch(t,
			[]string{"https://ttdev.au.auth0.com/userinfo1"},
			claims.Audience,
		)

		assert.NoError(t, claims.Valid())
	})

	t.Run("successfully unmarshal when there is no audience", func(t *testing.T) {
		input := `{
			"https://blackmagicdesign.com/userId": "72f19d99-4ab8-498e-9355-2b9f9c1f04c3"
		}`

		claims := auth.Claims{}
		err := json.Unmarshal([]byte(input), &claims)
		require.NoError(t, err)

		assert.Equal(t, uuid.MustParse("72f19d99-4ab8-498e-9355-2b9f9c1f04c3"), claims.UserID)
		assert.Nil(t, claims.Audience)

		assert.NoError(t, claims.Valid())
	})
}

func TestClaimsValid(t *testing.T) {
	t.Run("returns error on nil userID", func(t *testing.T) {
		input := `{
			"number": "1"
		}`

		claims := auth.Claims{}
		err := json.Unmarshal([]byte(input), &claims)

		require.NoError(t, err)
		assert.Error(t, claims.Valid())
	})

	t.Run("returns error when the token expires", func(t *testing.T) {
		expiresAt := time.Now().Unix() - 10

		input := fmt.Sprintf(`{
			"https://blackmagicdesign.com/userId": "72f19d99-4ab8-498e-9355-2b9f9c1f04c3",
			"exp": %d
		}`, expiresAt)

		claims := auth.Claims{}
		err := json.Unmarshal([]byte(input), &claims)

		require.NoError(t, err)
		assert.Error(t, claims.Valid())
	})
}
