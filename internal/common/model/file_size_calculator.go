package model

// Category indicates the file size group for the File Size Calculator handler
type Category string

// Category enum
const (
	// FileSizeCategorySmall indicates the Small category of file size between 0 and 1000 kB
	FileSizeCategorySmall = Category("SMALL")
	// FileSizeCategoryMedium indicates the Medium category of file size between 1001 and 3000 kB
	FileSizeCategoryMedium = Category("MEDIUM")
	// FileSizeCategoryLarge indicates the Large category of file size between 3001 and 5000 kB
	FileSizeCategoryLarge = Category("LARGE")
	// FileSizeCategoryXLarge indicates the X-Large category of file size greater than 5000 kB
	FileSizeCategoryXLarge = Category("X‐LARGE")
)
