package errutils

import (
	"errors"
	"fmt"
	"io"

	pkgerr "github.com/pkg/errors"
)

type chainedError struct {
	cause,
	err error
	*stack
}

// NewChainedError creates a new error that chains the given parent (cause) and child (err) together
func NewChainedError(cause, err error) error {
	return &chainedError{
		cause: cause,
		err:   err,
		stack: callers(),
	}
}

func (w *chainedError) Error() string {
	return fmt.Sprintf("%s: %s", w.err, w.cause)
}

// Format formats the chained error in the same way as `github.com/pkg/errors`
func (w *chainedError) Format(s fmt.State, verb rune) {
	switch verb {
	case 'v':
		if s.Flag('+') {
			fmt.Fprintf(s, "%+v\n", w.cause)
			io.WriteString(s, w.err.Error()) // nolint
			w.stack.Format(s, verb)
			return
		}
		fallthrough
	case 's':
		io.WriteString(s, w.Error()) // nolint
	case 'q':
		fmt.Fprintf(s, "%q", w.Error())
	}
}

func (w *chainedError) Cause() error {
	return w.cause
}

func (w *chainedError) Unwrap() error {
	return w.cause
}

func (w *chainedError) Is(target error) bool {
	return errors.Is(w.err, target) || errors.Is(w.cause, target)
}

func (w *chainedError) As(target interface{}) bool {
	return errors.As(w.err, target) || errors.As(w.cause, target)
}

func (w *chainedError) StackTrace() pkgerr.StackTrace {
	type stackTracer interface {
		StackTrace() pkgerr.StackTrace
	}

	causeST, ok := w.cause.(stackTracer)
	if !ok {
		return w.stack.StackTrace()
	}

	// if cause has stacktrace, we try to combine them together
	causeStack := causeST.StackTrace()
	currStack := w.stack.StackTrace()

	causeIdx := len(causeStack) - 1
	currIdx := len(currStack) - 1

	// try to find the common parts backwards
	for causeIdx >= 0 && currIdx >= 0 && causeStack[causeIdx] == currStack[currIdx] {
		causeIdx--
		currIdx--
	}

	// for edge cases, pick the one with more info
	if causeIdx < 0 {
		return currStack
	} else if currIdx < 0 {
		return causeStack
	}

	return append(causeStack[:causeIdx+1], currStack[currIdx:]...)
}
