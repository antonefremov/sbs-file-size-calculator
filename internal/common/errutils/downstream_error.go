package errutils

import "fmt"

type DownstreamError struct {
	Msg     string
	Err     error
	DestURL string
}

func (de *DownstreamError) Error() string {
	m := de.Msg
	if m == "" {
		m = de.Err.Error()
	}
	return fmt.Sprintf("Failed to call a downstream service on passed URL %s with error message: \n%s", de.DestURL, m)
}
