//nolint
// Code partially copied from https://github.com/pkg/errors/blob/v0.9.1/stack.go and modified based on that
package errutils

import (
	"fmt"
	"runtime"
	"strconv"
	"strings"

	pkgerr "github.com/pkg/errors"
)

func pc(f pkgerr.Frame) uintptr { return uintptr(f) - 1 }

// funcname removes the path prefix component of a function's name reported by func.Name().
func funcname(name string) string {
	i := strings.LastIndex(name, "/")
	name = name[i+1:]
	i = strings.Index(name, ".")
	return name[i+1:]
}

// StackTraceInfo returns the full path to the file, the line number of source code
// and the function name without the path prefix and package name.
func StackTraceInfo(f pkgerr.Frame) (file, line, name string) {
	file, line, name = "", "", ""

	fn := runtime.FuncForPC(pc(f))
	if fn == nil {
		return
	}

	fileStr, lineInt := fn.FileLine(pc(f))
	if fileStr != "" {
		file = fileStr
	}

	line = strconv.Itoa(lineInt)

	nameStr := fn.Name()
	if nameStr != "" {
		name = funcname(nameStr)
	}

	return
}

// stack represents a stack of program counters.
type stack []uintptr

func (s *stack) Format(st fmt.State, verb rune) {
	switch verb {
	case 'v':
		switch {
		case st.Flag('+'):
			for _, pc := range *s {
				f := pkgerr.Frame(pc)
				fmt.Fprintf(st, "\n%+v", f)
			}
		}
	}
}

func (s *stack) StackTrace() pkgerr.StackTrace {
	f := make([]pkgerr.Frame, len(*s))
	for i := 0; i < len(f); i++ {
		f[i] = pkgerr.Frame((*s)[i])
	}
	return f
}

func callers() *stack {
	const depth = 32
	var pcs [depth]uintptr
	n := runtime.Callers(3, pcs[:])
	var st stack = pcs[0:n]
	return &st
}
