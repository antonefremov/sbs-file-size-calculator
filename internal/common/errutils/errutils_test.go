package errutils

import (
	"errors"
	"testing"

	pkgerr "github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
)

func TestChainedError_Error(t *testing.T) {
	type fields struct {
		cause error
		err   error
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "correct error message",
			fields: fields{
				cause: errors.New("item doesn't exist in database"),
				err:   errors.New("not found"),
			},
			want: "not found: item doesn't exist in database",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := NewChainedError(tt.fields.cause, tt.fields.err)
			assert.Equal(t, tt.want, w.Error())
		})
	}
}

func assertErrorIsTheSameInstanceAs(as error) assert.ErrorAssertionFunc {
	return func(t assert.TestingT, err error, _ ...interface{}) bool {
		return err == as
	}
}

func TestChainedError_Cause(t *testing.T) {
	rootErr := errors.New("root error")

	type fields struct {
		cause error
		err   error
	}
	tests := []struct {
		name      string
		fields    fields
		assertion assert.ErrorAssertionFunc
	}{
		{
			name: "correct cause",
			fields: fields{
				cause: pkgerr.Wrap(rootErr, "intermediate message"),
				err:   errors.New("child error"),
			},
			assertion: assertErrorIsTheSameInstanceAs(rootErr),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := NewChainedError(tt.fields.cause, tt.fields.err)
			tt.assertion(t, pkgerr.Cause(w))
		})
	}
}

func TestChainedError_Unwrap(t *testing.T) {
	rootErr := errors.New("root error")

	type fields struct {
		cause error
		err   error
	}
	tests := []struct {
		name      string
		fields    fields
		assertion assert.ErrorAssertionFunc
	}{
		{
			name: "correct unwrap",
			fields: fields{
				cause: rootErr,
				err:   errors.New("child error"),
			},
			assertion: assertErrorIsTheSameInstanceAs(rootErr),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := NewChainedError(tt.fields.cause, tt.fields.err)
			tt.assertion(t, pkgerr.Unwrap(w))
		})
	}
}

func TestChainedError_Is(t *testing.T) {
	parentCauseErr := errors.New("item doesn't exist in database")
	childErr := errors.New("not found")

	type fields struct {
		cause error
		err   error
	}
	type args struct {
		target error
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "is on child error",
			fields: fields{
				cause: parentCauseErr,
				err:   childErr,
			},
			args: args{
				target: childErr,
			},
			want: true,
		},
		{
			name: "is on parent cause error",
			fields: fields{
				cause: parentCauseErr,
				err:   childErr,
			},
			args: args{
				target: parentCauseErr,
			},
			want: true,
		},
		{
			name: "is not",
			fields: fields{
				cause: parentCauseErr,
				err:   childErr,
			},
			args: args{
				target: errors.New("new error"),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := NewChainedError(tt.fields.cause, tt.fields.err)
			assert.Equal(t, tt.want, pkgerr.Is(w, tt.args.target))
		})
	}
}

type errType1 struct {
	msg string
}

func (e *errType1) Error() string {
	return e.msg
}

type errType2 struct {
	msg string
}

func (e *errType2) Error() string {
	return e.msg
}

type errType3 struct {
	msg string
}

func (e *errType3) Error() string {
	return e.msg
}

func TestChainedError_As(t *testing.T) {
	parentCauseTarget := &errType2{}
	childErrTarget := &errType1{}
	unknownTarget := &errType3{}

	type fields struct {
		cause error
		err   error
	}
	type args struct {
		target interface{}
	}
	tests := []struct {
		name               string
		fields             fields
		args               args
		want               bool
		wantedTargetErrMsg string
	}{
		{
			name: "as on child error",
			fields: fields{
				cause: &errType2{"parent cause error"},
				err:   &errType1{"child error"},
			},
			args: args{
				target: &childErrTarget,
			},
			want:               true,
			wantedTargetErrMsg: "child error",
		},
		{
			name: "as on parent cause error",
			fields: fields{
				cause: &errType2{"parent cause error"},
				err:   &errType1{"child error"},
			},
			args: args{
				target: &parentCauseTarget,
			},
			want:               true,
			wantedTargetErrMsg: "parent cause error",
		},
		{
			name: "as not",
			fields: fields{
				cause: &errType2{"parent cause error"},
				err:   &errType1{"child error"},
			},
			args: args{
				target: &unknownTarget,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := NewChainedError(tt.fields.cause, tt.fields.err)
			assert.Equal(t, tt.want, pkgerr.As(w, tt.args.target))
			switch v := tt.args.target.(type) {
			case **errType2:
				assert.EqualError(t, *v, tt.wantedTargetErrMsg)
			case **errType1:
				assert.EqualError(t, *v, tt.wantedTargetErrMsg)
			}
		})
	}
}
