package errutils

import (
	"fmt"
	"io"

	pkgerr "github.com/pkg/errors"
)

type PublicError interface {
	HTTPStatus() int
	Code() string
	Description() string
}

func WrapPublicError(err error, httpStatus int, code string, description string) error {
	if err == nil {
		return nil
	}

	return &publicError{
		err,
		httpStatus,
		code,
		description,
	}
}

func NewPublicErrorf(httpStatus int, code string, format string, arguments ...interface{}) error {
	return NewPublicError(httpStatus, code, fmt.Sprintf(format, arguments...))
}

func NewPublicError(httpStatus int, code string, description string) error {
	return &publicError{
		nil,
		httpStatus,
		code,
		description,
	}
}

func HasPublicError(err error) bool {
	return GetPublicError(err) != nil
}

func GetPublicError(err error) PublicError {
	var perr PublicError
	ok := pkgerr.As(err, &perr)
	if ok {
		return perr
	}

	return nil
}

type publicError struct {
	cause       error
	httpStatus  int
	code        string
	description string
}

func (pe *publicError) HTTPStatus() int {
	return pe.httpStatus
}

func (pe *publicError) Code() string {
	return pe.code
}

func (pe *publicError) Description() string {
	return pe.description
}

func (pe *publicError) Cause() error {
	return pe.cause
}

func (pe *publicError) Unwrap() error {
	return pe.cause
}

func (pe *publicError) Error() string {
	if pe.cause != nil {
		return pe.cause.Error()
	} // else
	return fmt.Sprintf("%s: %s", pe.code, pe.description)
}

func (pe *publicError) Format(s fmt.State, verb rune) {
	switch verb {
	case 'v':
		if s.Flag('+') {
			if pe.cause != nil {
				fmt.Fprintf(s, "%+v", pe.cause)
			} else {
				_, _ = io.WriteString(s, pe.Error())
			}
			return
		}
		fallthrough
	case 's':
		_, _ = io.WriteString(s, pe.Error())
	case 'q':
		fmt.Fprintf(s, "%q", pe.Error())
	}
}

func (pe *publicError) Is(target error) bool {
	p, ok := target.(PublicError)
	return ok && (p.Code() == pe.code &&
		p.HTTPStatus() == pe.httpStatus)
}

func (pe *publicError) StackTrace() pkgerr.StackTrace {
	type stackTracer interface {
		StackTrace() pkgerr.StackTrace
	}

	causeST, ok := pe.cause.(stackTracer)
	if !ok {
		return nil
	}

	return causeST.StackTrace()
}
