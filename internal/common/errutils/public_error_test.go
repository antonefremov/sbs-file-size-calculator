package errutils_test

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/errutils"
)

func TestPublicError(t *testing.T) {
	t.Run("Can create new public error", func(t *testing.T) {
		err := errutils.NewPublicError(http.StatusBadRequest, "Code", "Description")
		perr, ok := err.(errutils.PublicError)
		require.True(t, ok)
		assert.Equal(t, http.StatusBadRequest, perr.HTTPStatus())
		assert.Equal(t, "Code", perr.Code())
		assert.Equal(t, "Description", perr.Description())
		assert.Nil(t, errors.Cause(err))
		assert.Equal(t, "Code: Description", err.Error())
	})

	t.Run("Can wrap errors in a public error", func(t *testing.T) {
		ierr := errors.Errorf("Internal Error")
		err := errutils.WrapPublicError(ierr, http.StatusBadRequest, "Code", "Description")
		perr, ok := err.(errutils.PublicError)
		require.True(t, ok)
		assert.Equal(t, http.StatusBadRequest, perr.HTTPStatus())
		assert.Equal(t, "Code", perr.Code())
		assert.Equal(t, "Description", perr.Description())
		assert.Equal(t, ierr, errors.Cause(err))
		assert.Equal(t, "Internal Error", err.Error())
	})

	t.Run("Can get public errors that wrap other errors", func(t *testing.T) {
		ierr := errors.Errorf("Outer Error")
		err := errutils.WrapPublicError(ierr, http.StatusBadRequest, "Code", "Description")
		perr := errutils.GetPublicError(err)
		require.NotNil(t, perr)
		assert.Equal(t, http.StatusBadRequest, perr.HTTPStatus())
		assert.Equal(t, "Code", perr.Code())
		assert.Equal(t, "Description", perr.Description())
		assert.Equal(t, ierr, errors.Cause(err))
	})

	t.Run("Can get public errors that are wrapped by other errors", func(t *testing.T) {
		ierr := errutils.NewPublicError(http.StatusBadRequest, "Code", "Description")
		err := errors.Wrap(ierr, "Wrapper")
		perr := errutils.GetPublicError(err)
		require.NotNil(t, perr)
		assert.Equal(t, http.StatusBadRequest, perr.HTTPStatus())
		assert.Equal(t, "Code", perr.Code())
		assert.Equal(t, "Description", perr.Description())
		assert.Nil(t, errors.Cause(ierr))
	})

	t.Run("Returns nil when trying to get a public error from an internal error ", func(t *testing.T) {
		err := errors.Errorf("Error")
		perr := errutils.GetPublicError(err)
		require.Nil(t, perr)
	})

	t.Run("Printing a public errpr produces an error message", func(t *testing.T) {
		err := errutils.NewPublicError(http.StatusBadRequest, "Code", "Description")
		msg := fmt.Sprintf("%+v", err)
		assert.Equal(t, "Code: Description", msg)
	})
}
