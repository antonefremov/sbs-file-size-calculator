package middlewares_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/google/uuid"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/middlewares"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/middlewares/mocks"
)

func TestRequestAttributesLogHandlers(t *testing.T) {
	t.Run("log middleware correctly adds log fields", func(t *testing.T) {
		uuid := uuid.New()
		url := fmt.Sprintf("https:www.antonefremov.com/v1/blockchain/%s", uuid)

		out := &bytes.Buffer{}
		log.Logger = zerolog.New(out)

		w := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, url, nil)
		r.Header.Add("User-Agent", "test-user-agent")
		r.Header.Add("Referer", "test-referer")

		r = r.WithContext(log.Logger.WithContext(r.Context()))

		next := &mocks.Handler{}
		next.On("ServeHTTP",
			mock.MatchedBy(func(w *middlewares.StatusRecorder) bool {
				return true
			}),
			mock.Anything).
			Run(func(args mock.Arguments) {
				writter := args[0].(*middlewares.StatusRecorder)
				writter.WriteHeader(http.StatusBadRequest)
			}).
			Once()

		handler := middlewares.WithMiddlewares(next,
			middlewares.RequestAttributesLogHandlers(url),
			middlewares.RequestLogHandler,
		)

		handler.ServeHTTP(w, r)

		type logOutput struct {
			Level    string `json:"level"`
			Endpoint string `json:"endpoint"`
			Method   string `json:"method"`
			Agent    string `json:"userAgent"`
			Referer  string `json:"referer"`
			Event    string `json:"event"`
			Status   int    `json:"statusCode"`
		}

		logs := strings.SplitAfter(out.String(), "}")
		require.LessOrEqual(t, 2, len(logs))

		firstLog := &logOutput{}
		err := json.Unmarshal([]byte(logs[0]), firstLog)
		require.NoError(t, err)

		require.Equal(t, "info", firstLog.Level)
		require.Equal(t, url, firstLog.Endpoint)
		require.Equal(t, http.MethodGet, firstLog.Method)
		require.Equal(t, "test-user-agent", firstLog.Agent)
		require.Equal(t, "test-referer", firstLog.Referer)
		require.Equal(t, 0, firstLog.Status)
		require.Equal(t, "request", firstLog.Event)

		secondLog := &logOutput{}
		err = json.Unmarshal([]byte(logs[1]), secondLog)
		require.NoError(t, err)

		require.Equal(t, "info", secondLog.Level)
		require.Equal(t, url, secondLog.Endpoint)
		require.Equal(t, http.MethodGet, secondLog.Method)
		require.Equal(t, "test-user-agent", secondLog.Agent)
		require.Equal(t, "test-referer", secondLog.Referer)
		require.Equal(t, http.StatusBadRequest, secondLog.Status)
		require.Equal(t, "response", secondLog.Event)

		next.AssertExpectations(t)
	})
}
