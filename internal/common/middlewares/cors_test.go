package middlewares_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/handlers"
	"github.com/stretchr/testify/assert"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/middlewares"
)

func TestCORSHandler(t *testing.T) {
	t.Run("given OPTIONS request", func(t *testing.T) {
		h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			assert.Fail(t, "OPTIONS request must not be passed to next handler")
		})
		t.Run("when no configuration provided then sets default CORS headers in response", func(t *testing.T) {
			req := httptest.NewRequest("OPTIONS", "/", nil)
			req.Header.Set("Origin", "https://antonefremov.com")
			req.Header.Set("Access-Control-Request-Method", "POST")

			rr := httptest.NewRecorder()
			middlewares.CORSHandler()(h).ServeHTTP(rr, req)

			res := rr.Result()
			defer res.Body.Close()

			assert.Equal(t, http.StatusOK, res.StatusCode)
			assert.Equal(t, "*", res.Header.Get("Access-Control-Allow-Origin"))
			assert.Empty(t, res.Header.Values("Access-Control-Allow-Methods"))
			assert.Empty(t, res.Header.Get("Access-Control-Max-Age"))
		})

		t.Run("when configuration provided then sets custom CORS headers in response", func(t *testing.T) {
			oneMinute := 60
			req := httptest.NewRequest("OPTIONS", "/", nil)
			req.Header.Set("Origin", "https://antonefremov.com")
			req.Header.Set("Access-Control-Request-Method", "POST")

			rr := httptest.NewRecorder()
			middlewares.CORSHandler(
				handlers.AllowedMethods([]string{"DELETE", "POST"}),
				handlers.AllowedOrigins([]string{"https://antonefremov.com"}),
				handlers.MaxAge(oneMinute),
			)(h).ServeHTTP(rr, req)

			res := rr.Result()
			defer res.Body.Close()

			assert.Equal(t, http.StatusOK, res.StatusCode)
			assert.Equal(t, "https://antonefremov.com", res.Header.Get("Access-Control-Allow-Origin"))
			// NOTE: gorilla does not set Access-Control-Allow-Methods header
			// for default CORS Methods ["GET", "HEAD", "POST"]
			assert.Empty(t, res.Header.Values("Access-Control-Allow-Methods"))
			assert.Equal(t, "60", res.Header.Get("Access-Control-Max-Age"))
		})

		t.Run("when requested method not provided then responses HTTP status code 400", func(t *testing.T) {
			req := httptest.NewRequest("OPTIONS", "/", nil)
			req.Header.Set("Origin", "https://antonefremov.com")

			rr := httptest.NewRecorder()
			middlewares.CORSHandler(
				handlers.AllowedOrigins([]string{"https://antonefremov.com"}),
			)(h).ServeHTTP(rr, req)

			res := rr.Result()
			defer res.Body.Close()

			assert.Equal(t, http.StatusBadRequest, res.StatusCode)
		})

		t.Run("when requested method not allowed then responses HTTP status code 405", func(t *testing.T) {
			req := httptest.NewRequest("OPTIONS", "/", nil)
			req.Header.Set("Origin", "https://antonefremov.com")
			req.Header.Set("Access-Control-Request-Method", "PUT")

			rr := httptest.NewRecorder()
			middlewares.CORSHandler(
				handlers.AllowedOrigins([]string{"https://antonefremov.com"}),
			)(h).ServeHTTP(rr, req)

			res := rr.Result()
			defer res.Body.Close()

			assert.Equal(t, http.StatusMethodNotAllowed, res.StatusCode)
		})

		t.Run("when origin not allowed then responses without headers", func(t *testing.T) {
			req := httptest.NewRequest("OPTIONS", "/", nil)
			req.Header.Set("Origin", "localhost")
			req.Header.Set("Access-Control-Request-Method", "PUT")

			rr := httptest.NewRecorder()
			middlewares.CORSHandler(
				handlers.AllowedOrigins([]string{"https://antonefremov.com"}),
				handlers.AllowedMethods([]string{"PUT"}),
			)(h).ServeHTTP(rr, req)

			res := rr.Result()
			defer res.Body.Close()

			assert.Equal(t, http.StatusOK, res.StatusCode)
			assert.Empty(t, res.Header)
		})
	})

	t.Run("given NON OPTIONS request", func(t *testing.T) {
		h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "NEXT HANDLER CALLED")
		})

		t.Run("when requested then calls next handler", func(t *testing.T) {
			req := httptest.NewRequest("DELETE", "/", nil)
			req.Header.Set("Origin", "https://antonefremov.com")

			rr := httptest.NewRecorder()
			middlewares.CORSHandler(
				handlers.AllowedOrigins([]string{"https://antonefremov.com"}),
			)(h).ServeHTTP(rr, req)

			res := rr.Result()
			defer res.Body.Close()

			body, _ := ioutil.ReadAll(res.Body)

			assert.Equal(t, http.StatusOK, res.StatusCode)
			assert.Equal(t, "NEXT HANDLER CALLED", string(body))
			assert.Equal(t, "https://antonefremov.com", res.Header.Get("Access-Control-Allow-Origin"))
		})
	})
}
