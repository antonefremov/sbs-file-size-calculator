package middlewares

//go:generate go run github.com/vektra/mockery/v2/ --srcpkg=net/http --name=Handler --case=Snake
//go:generate go run github.com/vektra/mockery/v2/ --srcpkg=gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/authenticator --name=AuthenticatorIface --case=Snake

import (
	"context"
	"errors"
	"net/http"

	"github.com/google/uuid"
	"github.com/rs/zerolog/hlog"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/auth"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/authenticator"
)

type authInfoContextKey string

const (
	// FIXME: At this stage, we simply rely on X-API-Key to identify service to service requests.
	apiKeyHeader string = "X-API-Key" // nolint:gosec
	apiKeyString string = "TTAPIKey"  // nolint:gosec

	authInfoKey authInfoContextKey = "authInfo"
)

type AuthInfo struct {
	Source  RequestSource
	UserID  uuid.UUID
	Skipped bool
}

func AuthInfoFromClaims(c *auth.Claims) AuthInfo {
	return AuthInfo{UserID: c.UserID}
}

type AuthSettings struct {
	AllowedSources     []RequestSource
	SkipAuth           bool // If SkipAuth = true, requests from all sources are accepted and will not be validated
	AudienceIdentifier string
}

func AuthHandler(a authenticator.AuthenticatorIface, s *AuthSettings) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			r = (func() *http.Request {

				if s == nil || s.SkipAuth {
					hlog.FromRequest(r).Debug().
						Msg("request authentication and authorization is skipped")

					return WithAuthInfo(r, AuthInfo{Skipped: true})
				}

				key := r.Header.Get(apiKeyHeader)

				if key == apiKeyString {
					if findRequestSource(s.AllowedSources, RequestSourceService) {
						return WithAuthInfo(r, AuthInfo{Source: RequestSourceService, Skipped: false})
					}

					w.WriteHeader(http.StatusUnauthorized)
					return nil
				}

				// All request with malformed X-API-Key header are treated as user request
				if !findRequestSource(s.AllowedSources, RequestSourceUser) {
					w.WriteHeader(http.StatusUnauthorized)
					return nil
				}

				claims, err := a.AuthenticateRequest(r)
				if err != nil {
					hlog.FromRequest(r).Error().
						Err(err).
						Msg("failed to authenticate request")

					if errors.Is(err, authenticator.ErrorAuthenticatorInternalError) {
						w.WriteHeader(http.StatusInternalServerError)
					} else {
						w.WriteHeader(http.StatusUnauthorized)
					}

					return nil
				}

				if len(claims.Audience) > 0 {
					ok := false
					for _, a := range claims.Audience {
						if a == s.AudienceIdentifier {
							ok = true
							break
						}
					}

					if !ok {
						hlog.FromRequest(r).Error().
							Str("svcAudID", s.AudienceIdentifier).
							Strs("jwtAudIDs", claims.Audience).
							Msg("the service is not in JWT audience claim")

						w.WriteHeader(http.StatusUnauthorized)
						return nil
					}
				}

				info := AuthInfoFromClaims(claims)
				info.Source = RequestSourceUser
				info.Skipped = false

				// TODO: validate scopes and roles

				return WithAuthInfo(r, info)
			})()

			if r != nil {
				next.ServeHTTP(w, r)
			}
		})
	}
}

func WithAuthInfo(r *http.Request, info AuthInfo) *http.Request {
	ctx := context.WithValue(r.Context(), authInfoKey, &info)
	return r.WithContext(ctx)
}

func GetAuthInfo(ctx context.Context) AuthInfo {
	v := ctx.Value(authInfoKey)

	if info, ok := v.(*AuthInfo); ok && info != nil {
		return *info
	}

	return AuthInfo{}
}

func MarkServiceRequest(r *http.Request) {
	r.Header.Set(apiKeyHeader, apiKeyString)
}
