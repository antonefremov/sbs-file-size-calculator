package middlewares

import (
	"net/http"

	"github.com/gorilla/mux"
)

type MiddlewareChain struct {
	middlewares []mux.MiddlewareFunc
}

func NewMiddlewareChain(middlewares ...mux.MiddlewareFunc) MiddlewareChain {
	return MiddlewareChain{
		middlewares: append(([]mux.MiddlewareFunc)(nil), middlewares...),
	}
}

func (m MiddlewareChain) Handler(h http.Handler) http.Handler {
	if h == nil {
		h = http.DefaultServeMux
	}

	for index := range m.middlewares {
		h = m.middlewares[len(m.middlewares)-1-index](h)
	}
	return h
}

// WithMiddlewares adds middlewares to a handler and return new handler. Middlewares are called in
// the provided order before the provided handler `h`
func WithMiddlewares(h http.Handler, mws ...func(http.Handler) http.Handler) http.Handler {
	for i := len(mws) - 1; i >= 0; i-- {
		h = mws[i](h)
	}
	return h
}
