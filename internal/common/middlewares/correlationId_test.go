package middlewares_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/middlewares"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/middlewares/mocks"
)

func TestCorrelationID(t *testing.T) {
	t.Run("Correlation id is read from header", func(t *testing.T) {
		const correlationID string = "20ef7a5e-5424-4172-8341-6b2c877fc81b"

		w := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/", nil)
		r.Header.Add(middlewares.CorrelationIDHeader, correlationID)

		next := &mocks.Handler{}
		next.On("ServeHTTP", mock.Anything, mock.MatchedBy(func(r *http.Request) bool {
			id := middlewares.GetCorrelationID(r.Context())
			assert.Equal(t, correlationID, id)
			return true
		})).Once()

		handler := middlewares.CorrelationIDHandler(next)
		handler.ServeHTTP(w, r)

		assert.Equal(t, correlationID, w.Header().Get(middlewares.CorrelationIDHeader))
		next.AssertExpectations(t)
	})

	t.Run("Correlation id is generated when missing from the header", func(t *testing.T) {
		w := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/", nil)

		var id string

		next := &mocks.Handler{}
		next.On("ServeHTTP", mock.Anything, mock.MatchedBy(func(r *http.Request) bool {
			id = middlewares.GetCorrelationID(r.Context())
			assert.NoError(t, is.UUID.Validate(id))
			return true
		})).Once()

		handler := middlewares.CorrelationIDHandler(next)
		handler.ServeHTTP(w, r)

		assert.Equal(t, id, w.Header().Get(middlewares.CorrelationIDHeader))
		next.AssertExpectations(t)
	})
}
