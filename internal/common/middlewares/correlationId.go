package middlewares

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/hlog"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/logger"
)

//go:generate go run github.com/vektra/mockery/v2/ --srcpkg=net/http --name=Handler --case=Snake

type correlationIDContextKey string

const correlationIDKey correlationIDContextKey = "correlationID"

const CorrelationIDHeader string = "X-Correlation-Id"

func CorrelationIDHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := r.Header.Get(CorrelationIDHeader)

		log := hlog.FromRequest(r)

		if id == "" {
			newUUID, err := uuid.NewUUID()
			if err != nil {
				log.Warn().Err(err).Msg("failed to generate correlation ID")
			}

			id = newUUID.String()
		}

		if id != "" {
			w.Header().Add(CorrelationIDHeader, id)

			log.UpdateContext(func(c zerolog.Context) zerolog.Context {
				return c.Str(logger.KeyCorrelationID, id)
			})

			r = SetCorrelationID(r, id)
		}

		next.ServeHTTP(w, r)
	})
}

func SetCorrelationID(r *http.Request, id string) *http.Request {
	ctx := context.WithValue(r.Context(), correlationIDKey, &id)
	return r.WithContext(ctx)
}

func GetCorrelationID(ctx context.Context) string {
	v := ctx.Value(correlationIDKey)

	// a nil *string is managed to be added to the contxt, the ok is still true
	if id, ok := v.(*string); ok && id != nil {
		return *id
	}
	return ""
}
