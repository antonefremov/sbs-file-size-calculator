package middlewares

import (
	ghandlers "github.com/gorilla/handlers"
)

// CORSHandler is the CORS http handler
var CORSHandler = ghandlers.CORS
