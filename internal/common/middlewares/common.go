package middlewares

import (
	"fmt"
	"net/http"

	pkgerr "github.com/pkg/errors"
	"github.com/rs/zerolog/hlog"
)

type RequestSource string

const (
	RequestSourceUser    RequestSource = "User"
	RequestSourceService RequestSource = "Service"
)

func findRequestSource(sources []RequestSource, target RequestSource) bool {
	for _, s := range sources {
		if s == target {
			return true
		}
	}

	return false
}

// PanicRecoveryHandler middleware that recovers from a panic, logs the panic,
// writes http.StatusInternalServerError, and continues to the next handler.
func PanicRecoveryHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if v := recover(); v != nil {
				w.WriteHeader(http.StatusInternalServerError)

				hlog.FromRequest(r).Error().
					Err(pkgerr.New(fmt.Sprint(v))).
					Msg("recovery handler")
			}
		}()

		next.ServeHTTP(w, r)
	})
}

// ResponseHeadersHandler is a middleware to set response headers.
func ResponseHeadersHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}
