package middlewares_test

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/auth"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/authenticator"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/middlewares"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/middlewares/mocks"
)

func TestAuth(t *testing.T) {
	t.Run("Reject service request on demand", func(t *testing.T) {
		w := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/", nil)
		middlewares.MarkServiceRequest(r)

		next := &mocks.Handler{}

		handler := middlewares.AuthHandler(
			&mocks.AuthenticatorIface{},
			&middlewares.AuthSettings{
				nil,
				false,
				"https://ttdev.au.auth0.com/userinfo",
			})(next)
		handler.ServeHTTP(w, r)

		assert.Equal(t, http.StatusUnauthorized, w.Code)
		assert.True(t, next.AssertNotCalled(t, "ServeHTTP", w, r))
	})

	t.Run("Accept service request on demand", func(t *testing.T) {
		w := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/", nil)
		middlewares.MarkServiceRequest(r)

		next := &mocks.Handler{}

		verifyRequest := func(r *http.Request) bool {
			info := middlewares.GetAuthInfo(r.Context())
			return info.Source == middlewares.RequestSourceService && info.Skipped == false
		}
		next.On("ServeHTTP", mock.Anything, mock.MatchedBy(verifyRequest)).Once()

		handler := middlewares.AuthHandler(
			&mocks.AuthenticatorIface{},
			&middlewares.AuthSettings{
				[]middlewares.RequestSource{middlewares.RequestSourceService},
				false,
				"https://ttdev.au.auth0.com/userinfo",
			})(next)
		handler.ServeHTTP(w, r)

		next.AssertExpectations(t)
	})

	t.Run("Reject user request on demand", func(t *testing.T) {
		w := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/", nil)

		next := &mocks.Handler{}

		handler := middlewares.AuthHandler(
			&mocks.AuthenticatorIface{},
			&middlewares.AuthSettings{
				nil,
				false,
				"https://ttdev.au.auth0.com/userinfo",
			})(next)

		handler.ServeHTTP(w, r)

		assert.Equal(t, http.StatusUnauthorized, w.Code)
		assert.True(t, next.AssertNotCalled(t, "ServeHTTP", w, r))
	})

	t.Run("Accept and validate user request on demand, with multiple audiences in JWT", func(t *testing.T) {
		w := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/", nil)

		a := &mocks.AuthenticatorIface{}
		a.On("AuthenticateRequest", r).Return(
			&auth.Claims{
				UserID: uuid.Nil,
				Audience: []string{
					"https://ttdev.au.auth0.com/userinfo",
					"https://tt-v-appli-q57zfx43lgat-1922719955.ap-southeast-2.elb.amazonaws.com/v1/",
				},
			},
			nil).Once()

		next := &mocks.Handler{}

		verifyRequest := func(r *http.Request) bool {
			info := middlewares.GetAuthInfo(r.Context())
			return info.Source == middlewares.RequestSourceUser && info.Skipped == false
		}
		next.On("ServeHTTP", mock.Anything, mock.MatchedBy(verifyRequest)).Once()

		handler := middlewares.AuthHandler(
			a,
			&middlewares.AuthSettings{
				[]middlewares.RequestSource{middlewares.RequestSourceUser},
				false,
				"https://ttdev.au.auth0.com/userinfo",
			})(next)

		handler.ServeHTTP(w, r)

		a.AssertExpectations(t)
		next.AssertExpectations(t)
	})

	t.Run("Accept and validate user request on demand, with one audience in JWT", func(t *testing.T) {
		w := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/", nil)

		a := &mocks.AuthenticatorIface{}
		a.On("AuthenticateRequest", r).Return(
			&auth.Claims{
				UserID:   uuid.Nil,
				Audience: []string{"https://ttdev.au.auth0.com/userinfo"},
			},
			nil).Once()

		next := &mocks.Handler{}

		verifyRequest := func(r *http.Request) bool {
			info := middlewares.GetAuthInfo(r.Context())
			return info.Source == middlewares.RequestSourceUser && info.Skipped == false
		}
		next.On("ServeHTTP", mock.Anything, mock.MatchedBy(verifyRequest)).Once()

		handler := middlewares.AuthHandler(
			a,
			&middlewares.AuthSettings{
				[]middlewares.RequestSource{middlewares.RequestSourceUser},
				false,
				"https://ttdev.au.auth0.com/userinfo",
			})(next)

		handler.ServeHTTP(w, r)

		a.AssertExpectations(t)
		next.AssertExpectations(t)
	})

	t.Run("Accept and validate user request on demand, with no audience in JWT", func(t *testing.T) {
		w := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/", nil)

		a := &mocks.AuthenticatorIface{}
		a.On("AuthenticateRequest", r).Return(
			&auth.Claims{
				UserID: uuid.Nil,
			},
			nil).Once()

		next := &mocks.Handler{}

		verifyRequest := func(r *http.Request) bool {
			info := middlewares.GetAuthInfo(r.Context())
			return info.Source == middlewares.RequestSourceUser && info.Skipped == false
		}
		next.On("ServeHTTP", mock.Anything, mock.MatchedBy(verifyRequest)).Once()

		handler := middlewares.AuthHandler(
			a,
			&middlewares.AuthSettings{
				[]middlewares.RequestSource{middlewares.RequestSourceUser},
				false,
				"https://ttdev.au.auth0.com/userinfo",
			})(next)

		handler.ServeHTTP(w, r)

		a.AssertExpectations(t)
		next.AssertExpectations(t)
	})

	t.Run("Skip validation on demand", func(t *testing.T) {
		w := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/", nil)

		a := &mocks.AuthenticatorIface{}

		next := &mocks.Handler{}

		verifyRequest := func(r *http.Request) bool {
			info := middlewares.GetAuthInfo(r.Context())
			return info.Skipped == true
		}
		next.On("ServeHTTP", mock.Anything, mock.MatchedBy(verifyRequest)).Once()

		handler := middlewares.AuthHandler(
			a,
			&middlewares.AuthSettings{
				[]middlewares.RequestSource{},
				true,
				"https://ttdev.au.auth0.com/userinfo",
			})(next)

		handler.ServeHTTP(w, r)

		assert.True(t, a.AssertNotCalled(t, "AuthenticateRequest", r))
		next.AssertExpectations(t)
	})

	t.Run("Accept user request and skip user request validation when passing a nil ptr to AuthSenttings", func(t *testing.T) {
		w := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/", nil)

		a := &mocks.AuthenticatorIface{}

		verifyRequest := func(r *http.Request) bool {
			info := middlewares.GetAuthInfo(r.Context())
			return info.Skipped == true
		}

		next := &mocks.Handler{}
		next.On("ServeHTTP", mock.Anything, mock.MatchedBy(verifyRequest)).Once()

		handler := middlewares.AuthHandler(a, nil)(next)
		handler.ServeHTTP(w, r)

		assert.True(t, a.AssertNotCalled(t, "AuthenticateRequest", r))
		next.AssertExpectations(t)
	})

	t.Run("Accept service request when passing a nil ptr to AuthSenttings", func(t *testing.T) {
		w := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/", nil)
		middlewares.MarkServiceRequest(r)

		next := &mocks.Handler{}

		verifyRequest := func(r *http.Request) bool {
			info := middlewares.GetAuthInfo(r.Context())
			return info.Skipped == true
		}
		next.On("ServeHTTP", mock.Anything, mock.MatchedBy(verifyRequest)).Once()

		handler := middlewares.AuthHandler(&mocks.AuthenticatorIface{}, nil)(next)
		handler.ServeHTTP(w, r)

		next.AssertExpectations(t)
	})

	t.Run("Stop and return 500 on authenticator internal error", func(t *testing.T) {
		w := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/", nil)

		a := &mocks.AuthenticatorIface{}
		a.On("AuthenticateRequest", r).Return(nil, authenticator.ErrorAuthenticatorInternalError).Once()

		next := &mocks.Handler{}

		handler := middlewares.AuthHandler(
			a,
			&middlewares.AuthSettings{
				[]middlewares.RequestSource{middlewares.RequestSourceUser, middlewares.RequestSourceService},
				false,
				"https://ttdev.au.auth0.com/userinfo",
			})(next)
		handler.ServeHTTP(w, r)

		assert.Equal(t, http.StatusInternalServerError, w.Code)
		assert.True(t, next.AssertNotCalled(t, "ServeHTTP", w, r))
	})

	t.Run("Stop and return 401 on other errors", func(t *testing.T) {
		w := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/", nil)

		a := &mocks.AuthenticatorIface{}
		a.On("AuthenticateRequest", r).Return(nil, errors.New("validation error")).Once()

		next := &mocks.Handler{}

		handler := middlewares.AuthHandler(
			a,
			&middlewares.AuthSettings{
				[]middlewares.RequestSource{middlewares.RequestSourceUser, middlewares.RequestSourceService},
				false,
				"https://ttdev.au.auth0.com/userinfo",
			})(next)
		handler.ServeHTTP(w, r)

		assert.Equal(t, http.StatusUnauthorized, w.Code)
		assert.True(t, next.AssertNotCalled(t, "ServeHTTP", w, r))
	})

	t.Run("Returns 401 if service is not in JWT recipient claim", func(t *testing.T) {
		w := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/", nil)

		a := &mocks.AuthenticatorIface{}
		a.On("AuthenticateRequest", r).Return(
			&auth.Claims{
				UserID: uuid.Nil,
				Audience: []string{
					"https://tt/userinfo",
					"https://tt-v-appli-q57zfx43lgat-1922719955.ap-southeast-2.elb.amazonaws.com/v1/",
				},
			},
			nil).Once()

		next := &mocks.Handler{}

		handler := middlewares.AuthHandler(
			a,
			&middlewares.AuthSettings{
				[]middlewares.RequestSource{middlewares.RequestSourceUser},
				false,
				"https://ttdev.au.auth0.com/userinfo",
			})(next)

		handler.ServeHTTP(w, r)

		a.AssertExpectations(t)
		assert.Equal(t, http.StatusUnauthorized, w.Code)
		assert.True(t, next.AssertNotCalled(t, "ServeHTTP", w, r))
	})
}
