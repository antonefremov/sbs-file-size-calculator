package middlewares

import (
	"bufio"
	"errors"
	"net"
	"net/http"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/hlog"
)

type StatusRecorder struct {
	http.ResponseWriter
	status int
}

func (w *StatusRecorder) WriteHeader(status int) {
	w.status = status
	w.ResponseWriter.WriteHeader(status)
}

func (w *StatusRecorder) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	h, ok := w.ResponseWriter.(http.Hijacker)
	if !ok {
		return nil, nil, errors.New("hijack not supported")
	}
	return h.Hijack()
}

func RequestAttributesLogHandlers(path string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return WithMiddlewares(next,
			endpointLogHandler(path),
			hlog.MethodHandler("method"),
		)
	}
}

func endpointLogHandler(path string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log := hlog.FromRequest(r)
			log.UpdateContext(func(c zerolog.Context) zerolog.Context {
				return c.Str("endpoint", path)
			})

			next.ServeHTTP(w, r)
		})
	}
}

func RequestLogHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log := hlog.FromRequest(r).With().
			Str("userAgent", r.Header.Get("User-Agent")).
			Str("referer", r.Header.Get("Referer")).
			Str("remoteAddr", r.RemoteAddr).
			Logger()

		recorder := &StatusRecorder{
			ResponseWriter: w,
			status:         200,
		}

		log.Info().Str("event", "request").Msg("")

		next.ServeHTTP(recorder, r)

		log.Info().
			Int("statusCode", recorder.status).
			Str("event", "response").
			Msg("")
	})
}
