package server

import (
	"context"
	"errors"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/logger"
)

const oneMB int = 1 << 20
const defaultHTTPAddress string = ":8080"
const (
	tenSeconds time.Duration = 10 * time.Second // nolint
	oneMinunte time.Duration = time.Minute
)

type serverOptions struct {
	name                  string
	router                http.Handler
	httpAddress           string
	httpReadTimeout       time.Duration
	httpWriteTimeout      time.Duration
	httpIdleTimeout       time.Duration
	httpReadHeaderTimeout time.Duration
	httpShutdownTimeout   time.Duration
	httpMaxHeaderBytes    int
}

var defaultServerOptions = serverOptions{
	httpAddress:           defaultHTTPAddress,
	httpReadTimeout:       tenSeconds,
	httpWriteTimeout:      tenSeconds,
	httpIdleTimeout:       oneMinunte,
	httpReadHeaderTimeout: tenSeconds,
	httpShutdownTimeout:   tenSeconds,
	httpMaxHeaderBytes:    oneMB,
}

type fwdToZerologWriter struct {
	logger *zerolog.Logger
}

func (fw *fwdToZerologWriter) Write(p []byte) (n int, err error) {
	fw.logger.Error().
		Str(logger.KeyComponent, "net/http").
		Msg(string(p))
	return len(p), nil
}

// Server defines the server struct
type Server struct {
	logger  *zerolog.Logger
	opts    serverOptions
	srv     *http.Server
	stopWgs []*sync.WaitGroup
	stopFns []func()
}

func New(logger *zerolog.Logger, opt ...Option) *Server {
	opts := defaultServerOptions
	for _, o := range opt {
		o.apply(&opts)
	}

	s := &Server{
		logger:  logger,
		opts:    opts,
		stopWgs: []*sync.WaitGroup{},
		stopFns: []func(){},
	}
	return s
}

func (s *Server) Start() {
	if s.srv != nil {
		return
	}

	s.srv = &http.Server{
		Addr:              s.opts.httpAddress,
		Handler:           s.opts.router,
		ReadTimeout:       s.opts.httpReadTimeout,
		WriteTimeout:      s.opts.httpWriteTimeout,
		IdleTimeout:       s.opts.httpIdleTimeout,
		ReadHeaderTimeout: s.opts.httpReadHeaderTimeout,
		MaxHeaderBytes:    s.opts.httpMaxHeaderBytes,
		ErrorLog:          log.New(&fwdToZerologWriter{s.logger}, "", 0),
	}

	errs := make(chan error, 1)
	go func() {
		errs <- s.srv.ListenAndServe()
	}()

	s.logger.Info().Msgf("service is listening at: %s", s.opts.httpAddress)

	osSignals := make(chan os.Signal, 1)
	signal.Notify(osSignals, os.Interrupt, syscall.SIGTERM)

	s.srv.RegisterOnShutdown(func() {
		for _, fn := range s.stopFns {
			fn()
		}
	})

	select {
	case err := <-errs:
		if errors.Is(err, http.ErrServerClosed) {
			s.logger.Info().Msg("closed service")
		} else {
			s.logger.Fatal().Err(err).Msg("failed to start service")
		}
	case sig := <-osSignals:
		s.logger.Debug().
			Stringer("signal", sig).
			Msg("termination signal received")
		s.Stop()
	}

	for _, wg := range s.stopWgs {
		wg.Wait()
	}

	s.logger.Info().Msg("service RIP")
}

func (s *Server) Stop() {
	srv := s.srv
	s.srv = nil

	if srv != nil {
		s.logger.Info().Msg("shutting down service...")
		ctx, cancel := context.WithTimeout(context.Background(), s.opts.httpShutdownTimeout)
		defer cancel()

		if err := srv.Shutdown(ctx); err != nil {
			s.logger.Info().Err(err).Msg("service shutdown with error")
		}
	}
}

func (s *Server) OnStop(fn func(), wg *sync.WaitGroup) {
	// If provided, `wg` will prevent `Start()` from returning until it counts down to zero
	// Otherwise, no effort is made to wait once `fn` is called
	if wg != nil {
		s.stopWgs = append(s.stopWgs, wg)
	}

	s.stopFns = append(s.stopFns, fn)
}

type Option interface {
	apply(*serverOptions)
}

type funcServerOption struct {
	f func(*serverOptions)
}

func (fo *funcServerOption) apply(o *serverOptions) {
	fo.f(o)
}

func newFuncServerOption(f func(*serverOptions)) *funcServerOption {
	return &funcServerOption{f}
}

func WithName(n string) Option {
	return newFuncServerOption(func(o *serverOptions) {
		o.name = n
	})
}

func WithRouter(r http.Handler) Option {
	return newFuncServerOption(func(o *serverOptions) {
		o.router = r
	})
}

func WithHTTPAddress(a string) Option {
	return newFuncServerOption(func(o *serverOptions) {
		o.httpAddress = a
	})
}

func WithHTTPReadTimeout(t int) Option {
	return newFuncServerOption(func(o *serverOptions) {
		o.httpReadTimeout = time.Duration(t) * time.Second
	})
}

func WithHTTPWriteTimeout(t int) Option {
	return newFuncServerOption(func(o *serverOptions) {
		o.httpWriteTimeout = time.Duration(t) * time.Second
	})
}

func WithHTTPIdleTimeout(t int) Option {
	return newFuncServerOption(func(o *serverOptions) {
		o.httpIdleTimeout = time.Duration(t) * time.Second
	})
}

func WithHTTPReadHeaderTimeout(t int) Option {
	return newFuncServerOption(func(o *serverOptions) {
		o.httpReadHeaderTimeout = time.Duration(t) * time.Second
	})
}

func WithHTTPShutdownTimeout(t int) Option {
	return newFuncServerOption(func(o *serverOptions) {
		o.httpShutdownTimeout = time.Duration(t) * time.Second
	})
}

func WithHTTPMaxHeaderBytes(b int) Option {
	return newFuncServerOption(func(o *serverOptions) {
		o.httpMaxHeaderBytes = b
	})
}
