package server_test

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/router"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/server"
)

func TestServer(t *testing.T) {
	if isCI := os.Getenv("CI"); isCI != "" {
		t.Skip("skipping test in CI due to container networking issues")
	}

	serverName := "testServer"

	logOutput := &bytes.Buffer{}
	log.Logger = zerolog.New(logOutput).With().Logger()

	r := router.New()
	s := server.New(&log.Logger, server.WithName(serverName), server.WithRouter(r), server.WithHTTPAddress(":9999"))

	go func() {
		s.Start()
		defer s.Stop()
	}()

	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
	defer cancel()

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "http://127.0.0.1:9999/health", nil)
	require.NoError(t, err)

	client := http.DefaultClient

	time.Sleep(10 * time.Millisecond) // let server start before making request
	res, err := client.Do(req)
	require.NoError(t, err)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	assert.Equal(t, http.StatusOK, res.StatusCode)
	assert.JSONEq(t, `{"status": "OK"}`, string(body))

	type log struct {
		Level string `json:"level"`
		Msg   string `json:"message"`
	}

	logStruct := &log{}
	err = json.Unmarshal(logOutput.Bytes(), logStruct)
	require.NoError(t, err)

	logMsg := "service is listening at: :9999"
	assert.Equal(t, logMsg, logStruct.Msg)
	assert.Equal(t, "info", logStruct.Level)
}
