package authenticator

import (
	"context"
	"crypto/rsa"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/lestrrat-go/jwx/jwa"
	"github.com/lestrrat-go/jwx/jwk"
	"github.com/patrickmn/go-cache"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/auth"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/errutils"
)

const jwtEncryptionAlgorithm string = "RS256"

var ErrorAuthenticatorInternalError = errors.New("authenticator internal error")

type AuthenticatorIface interface { // nolint:revive
	AuthenticateRequest(r *http.Request) (*auth.Claims, error)
}

type authenticator struct {
	jwksEndpoint string
	cache        *cache.Cache
}

func New(jwksEndpoint string) AuthenticatorIface {
	cache := cache.New(5*time.Minute, 10*time.Minute)

	return &authenticator{jwksEndpoint: jwksEndpoint, cache: cache}
}

func (a *authenticator) AuthenticateRequest(r *http.Request) (*auth.Claims, error) {
	tokenStr := r.Header.Get("Authorization")
	if !strings.HasPrefix(strings.ToLower(tokenStr), "bearer ") {
		return nil, errors.New("invalid authorization header")
	}

	// Authorization header value is formatted as : "Bearer <JWT string>""
	tokenStr = strings.Split(tokenStr, " ")[1]

	keyFunc := jwt.Keyfunc(func(token *jwt.Token) (interface{}, error) {
		kid, err := validateJWTHeaders(token)
		if err != nil {
			return nil, err
		}

		// First, find the jwk from cache
		if jwk, found := a.cache.Get(kid); found && jwk != nil {
			return jwk, nil
		}

		// If jwk not found in cache, get JWKS from auth 0
		err = a.fetchJWKS()
		if err != nil {
			return nil, err // jwk-go will wrap external error err into ValidationError as {Inner: err, Errors: ValidationErrorUnverifiable}
		}

		if jwk, found := a.cache.Get(kid); found && jwk != nil {
			return jwk, nil
		}

		return nil, jwt.NewValidationError("Couldn't find public key", jwt.ValidationErrorUnverifiable)
	})

	token, err := jwt.ParseWithClaims(tokenStr, &auth.Claims{}, keyFunc)
	if err != nil {
		if ve, ok := err.(jwt.ValidationError); ok && errors.Is(ve.Inner, ErrorAuthenticatorInternalError) {
			return nil, ve.Inner
		}

		return nil, err
	}

	claims, ok := token.Claims.(*auth.Claims)
	if !ok || !token.Valid {
		return nil, errors.New("invalid claim")
	}

	return claims, nil
}

func (a *authenticator) fetchJWKS() error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	set, err := jwk.Fetch(ctx, a.jwksEndpoint)
	if err != nil {
		return errutils.NewChainedError(err, ErrorAuthenticatorInternalError)
	}

	for it := set.Iterate(context.Background()); it.Next(context.Background()); {
		pair := it.Pair()
		key := pair.Value.(jwk.Key)

		// alg is optional, see https://datatracker.ietf.org/doc/html/rfc7517#section-4.1
		if key.KeyType() != jwa.RSA || (key.Algorithm() != "" && key.Algorithm() != jwtEncryptionAlgorithm) || key.KeyUsage() != "sig" || len(key.KeyID()) == 0 {
			continue // Drop this item
		}

		pk := rsa.PublicKey{}
		if err := key.Raw(&pk); err != nil {
			continue // Drop this item
		}
		// Add valid JWK to the cache with cache's default expiration time
		a.cache.Set(key.KeyID(), &pk, 0)
	}

	return nil
}

func validateJWTHeaders(token *jwt.Token) (kid string, err error) {
	alg, ok := token.Header["alg"]
	if !ok {
		return "", jwt.NewValidationError("Missing alg in the JWT header", jwt.ValidationErrorMalformed)
	}

	if alg != jwtEncryptionAlgorithm {
		msg := fmt.Sprintf("Invalid encryption algorithm, expecting - %s, actual - %s", jwtEncryptionAlgorithm, alg)
		return "", jwt.NewValidationError(msg, jwt.ValidationErrorMalformed)
	}

	kidIface, ok := token.Header["kid"]
	if !ok || kidIface == "" {
		return "", jwt.NewValidationError("Missing kid in the JWT header", jwt.ValidationErrorMalformed)
	}

	return kidIface.(string), nil
}
