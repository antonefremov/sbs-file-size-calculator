package authenticator_test

import (
	"crypto/rand"
	"crypto/rsa"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"github.com/lestrrat-go/jwx/jwk"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/auth"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/authenticator"
)

func generateJWKAndJWT(kid string, userID uuid.UUID, aud []string) (jwkObj jwk.Key, jwtString string, err error) {
	// Generate a JWK
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, "", err
	}

	key, err := jwk.New(privateKey.PublicKey)
	if err != nil {
		return nil, "", err
	}
	if kid != "" {
		err = key.Set(jwk.KeyIDKey, kid)
		if err != nil {
			return nil, "", err
		}
	}

	err = key.Set(jwk.KeyUsageKey, "sig")
	if err != nil {
		return nil, "", err
	}

	// Use the same private key to sign a JWT
	claims := auth.Claims{
		UserID:   userID,
		Audience: aud,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)

	if kid != "" {
		token.Header["kid"] = kid
	}

	tokenStr, err := token.SignedString(privateKey)
	if err != nil {
		return nil, "", err
	}

	return key, tokenStr, nil
}

func generateMockServer(t *testing.T, jwkObjs ...jwk.Key) *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		set := jwk.NewSet()

		for _, obj := range jwkObjs {
			added := set.Add(obj)
			require.True(t, added)
		}

		jsonBuf, err := json.Marshal(set)
		require.NoError(t, err)

		w.WriteHeader(http.StatusOK)
		bytes, err := w.Write(jsonBuf)
		require.NoError(t, err)
		require.Equal(t, len(jsonBuf), bytes)
	}))
}

func TestAuthenticateRequest(t *testing.T) {
	userID := uuid.New()
	aud := []string{
		"https://ttdev.au.auth0.com/userinfo",
		"https://tt-v-appli-q57zfx43lgat-1922719955.ap-southeast-2.elb.amazonaws.com/v1/",
	}

	t.Run("Authorization header prefix should be case-insensitive", func(t *testing.T) {
		jwkObj, tokenStr, err := generateJWKAndJWT("test-kid-1", userID, aud)
		require.NoError(t, err)

		ts := generateMockServer(t, jwkObj)
		defer ts.Close()

		a := authenticator.New(ts.URL)

		r := httptest.NewRequest(http.MethodGet, "/", nil)
		r.Header.Add("Authorization", fmt.Sprintf("BeArer %s", tokenStr))

		cs, err := a.AuthenticateRequest(r)

		assert.NoError(t, err)
		assert.Equal(t, &auth.Claims{UserID: userID, Audience: aud}, cs)
	})

	t.Run("Request JWKS from Auth0 then parse and verify the JWT", func(t *testing.T) {
		jwkObj, tokenStr, err := generateJWKAndJWT("test-kid-1", userID, aud)
		require.NoError(t, err)

		ts := generateMockServer(t, jwkObj)
		defer ts.Close()

		a := authenticator.New(ts.URL)

		r := httptest.NewRequest(http.MethodGet, "/", nil)
		r.Header.Add("Authorization", fmt.Sprintf("Bearer %s", tokenStr))

		cs, err := a.AuthenticateRequest(r)

		assert.NoError(t, err)
		assert.Equal(t, &auth.Claims{UserID: userID, Audience: aud}, cs)
	})

	t.Run("Store JWK in cache and use JWKs in cache for future requests", func(t *testing.T) {
		userID := uuid.New()
		jwkObj, tokenStr, err := generateJWKAndJWT("test-kid-1", userID, aud)
		require.NoError(t, err)

		ts := generateMockServer(t, jwkObj)
		defer ts.Close()

		a := authenticator.New(ts.URL)

		r := httptest.NewRequest(http.MethodGet, "/", nil)
		r.Header.Add("Authorization", fmt.Sprintf("Bearer %s", tokenStr))

		cs, err := a.AuthenticateRequest(r)

		assert.NoError(t, err)
		expectedClaims := &auth.Claims{UserID: userID, Audience: aud}
		assert.Equal(t, expectedClaims, cs)

		// Close the server then verify again
		ts.Close()

		cs, err = a.AuthenticateRequest(r)

		assert.NoError(t, err)
		assert.Equal(t, expectedClaims, cs)
	})

	t.Run("Returns authenticator internal error if fails to get JWKS from server", func(t *testing.T) {
		userID := uuid.New()
		_, tokenStr, err := generateJWKAndJWT("test-kid-1", userID, aud)
		require.NoError(t, err)

		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusBadRequest)
		}))

		defer ts.Close()

		a := authenticator.New(ts.URL)

		r := httptest.NewRequest(http.MethodGet, "/", nil)
		r.Header.Add("Authorization", fmt.Sprintf("Bearer %s", tokenStr))

		cs, err := a.AuthenticateRequest(r)

		assert.Error(t, err)
		assert.Nil(t, cs)
	})

	t.Run("Returns error if JWT header has no kid field", func(t *testing.T) {
		userID := uuid.New()
		jwkObj, tokenStr, err := generateJWKAndJWT("", userID, aud)
		require.NoError(t, err)

		ts := generateMockServer(t, jwkObj)
		defer ts.Close()

		a := authenticator.New(ts.URL)

		r := httptest.NewRequest(http.MethodGet, "/", nil)
		r.Header.Add("Authorization", fmt.Sprintf("Bearer %s", tokenStr))

		cs, err := a.AuthenticateRequest(r)

		assert.Error(t, err)
		assert.Nil(t, cs)
	})

	t.Run("Returns error if JWT algorithm is not RS256", func(t *testing.T) {
		signingKey := []byte("TTTestKey")
		token := jwt.New(jwt.SigningMethodHS256)
		tokenStr, err := token.SignedString(signingKey)
		require.NoError(t, err)

		a := authenticator.New("")

		r := httptest.NewRequest(http.MethodGet, "/", nil)
		r.Header.Add("Authorization", fmt.Sprintf("Bearer %s", tokenStr))

		cs, err := a.AuthenticateRequest(r)

		assert.Error(t, err)
		assert.Nil(t, cs)
	})

	t.Run("Returns error if no JWK is found", func(t *testing.T) {
		userID := uuid.New()
		_, tokenStr, err := generateJWKAndJWT("test-kid-1", userID, aud)
		require.NoError(t, err)

		jwkObj, _, err := generateJWKAndJWT("test-kid-2", userID, aud)
		require.NoError(t, err)

		ts := generateMockServer(t, jwkObj)
		defer ts.Close()

		a := authenticator.New(ts.URL)

		r := httptest.NewRequest(http.MethodGet, "/", nil)
		r.Header.Add("Authorization", fmt.Sprintf("Bearer %s", tokenStr))

		cs, err := a.AuthenticateRequest(r)

		assert.Error(t, err)
		assert.Nil(t, cs)
	})

	t.Run("Returns error if the Authorization header is missing", func(t *testing.T) {
		a := authenticator.New("")

		r := httptest.NewRequest(http.MethodGet, "/", nil)

		cs, err := a.AuthenticateRequest(r)
		assert.Error(t, err)
		assert.Nil(t, cs)
	})

	t.Run("Returns error if the Authorization header is malformed", func(t *testing.T) {
		a := authenticator.New("")

		r := httptest.NewRequest(http.MethodGet, "/", nil)
		r.Header.Add("Authorization", "Token invalid-token")

		cs, err := a.AuthenticateRequest(r)
		assert.Error(t, err)
		assert.Nil(t, cs)
	})
}
