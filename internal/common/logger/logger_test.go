package logger_test

import (
	"bytes"
	"encoding/json"
	"testing"

	"github.com/pkg/errors"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/logger"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/logger/mocks"
)

func TestLogger(t *testing.T) {
	t.Run("Init() method returns a logger with service name, env, commit hash, and error stack by default", func(t *testing.T) {
		out := &bytes.Buffer{}
		log.Logger = zerolog.New(out)

		cfg := &mocks.Cfg{}
		cfg.On("ServiceName").Return("presentation-service").Once()
		cfg.On("LogLevel").Return(zerolog.InfoLevel).Twice()
		cfg.On("Env").Return("dev").Twice()
		cfg.On("CommitHash").Return("abcd123").Once()

		type logOutput struct {
			Env     string        `json:"env"`
			SrvName string        `json:"svc"`
			CmtHsh  string        `json:"commitHash"`
			Stack   []interface{} `json:"stack"`
		}

		log := logger.Init(cfg)
		require.NotNil(t, log)

		log.Info().Err(errors.New("some error")).Msg("")

		logStruct := &logOutput{}
		err := json.Unmarshal(out.Bytes(), logStruct)
		require.NoError(t, err)

		require.Equal(t, zerolog.InfoLevel, log.GetLevel())
		require.Equal(t, "dev", logStruct.Env)
		require.Equal(t, "presentation-service", logStruct.SrvName)
		require.Equal(t, "abcd123", logStruct.CmtHsh)
		require.NotEqual(t, 0, len(logStruct.Stack))

		cfg.AssertExpectations(t)
	})

	t.Run("Init() method correctly set the log level", func(t *testing.T) {
		out := &bytes.Buffer{}
		log.Logger = zerolog.New(out)

		cfg := &mocks.Cfg{}
		cfg.On("ServiceName").Return("presentation-service").Once()
		cfg.On("LogLevel").Return(zerolog.FatalLevel).Twice()
		cfg.On("Env").Return("dev").Twice()
		cfg.On("CommitHash").Return("abcd123").Once()

		log := logger.Init(cfg)
		require.NotNil(t, log)

		log.Info().Err(errors.New("some error")).Msg("")

		require.Equal(t, 0, len(out.Bytes()))

		cfg.AssertExpectations(t)
	})

	t.Run("log has caller info if cfg log level is below debug", func(t *testing.T) {
		out := &bytes.Buffer{}
		log.Logger = zerolog.New(out)

		cfg := &mocks.Cfg{}
		cfg.On("ServiceName").Return("presentation-service").Once()
		cfg.On("LogLevel").Return(zerolog.DebugLevel).Twice()
		cfg.On("Env").Return("dev").Twice()
		cfg.On("CommitHash").Return("abcd123").Once()

		type logOutput struct {
			Caller  string `json:"caller"`
			Env     string `json:"env"`
			SrvName string `json:"svc"`
			CmtHsh  string `json:"commitHash"`
		}

		log := logger.Init(cfg)
		require.NotNil(t, log)

		log.Info().Msg("")

		logStruct := &logOutput{}
		err := json.Unmarshal(out.Bytes(), logStruct)
		require.NoError(t, err)

		require.Equal(t, zerolog.DebugLevel, log.GetLevel())
		require.NotEmpty(t, logStruct.Caller)
		require.Equal(t, "dev", logStruct.Env)
		require.Equal(t, "presentation-service", logStruct.SrvName)
		require.Equal(t, "abcd123", logStruct.CmtHsh)

		cfg.AssertExpectations(t)
	})
}

func TestMarshalStack(t *testing.T) {
	t.Run("Correctly marshal an error with stack trace", func(t *testing.T) {
		err := errors.New("an error with stact trace")
		stacks := logger.MarshalStack(err)

		stacksMaps, ok := stacks.([]map[string]string)
		assert.True(t, ok)

		assert.NotEqual(t, 0, len(stacksMaps))

		for _, m := range stacksMaps {
			assert.Equal(t, 3, len(m))

			for _, value := range m {
				assert.NotEmpty(t, value)
			}
		}
	})
}
