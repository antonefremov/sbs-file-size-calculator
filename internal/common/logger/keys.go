package logger

const (
	// Keys for logging common fields
	KeyAPIVersion    = "apiVersion"
	KeyCorrelationID = "correlationId"
	KeyTraceID       = "traceId"

	// Used to indicate which package/library/etc. a message comes from
	// This should be used when the logger is passed into a 3rd party library
	KeyComponent = "component"

	// Resource keys
	KeyPresentationID = "presentationId"
	KeyClipID         = "clipId"
)
