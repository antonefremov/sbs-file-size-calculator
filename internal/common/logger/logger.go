package logger

//go:generate go run github.com/vektra/mockery/v2/ --name=Cfg --case=Snake

import (
	"os"

	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/errutils"
)

const (
	highestLevelWithCallerInfo zerolog.Level = zerolog.DebugLevel

	stackSourceFileName     = "source"
	stackSourceLineName     = "line"
	stackSourceFunctionName = "func"
)

type Cfg interface {
	ServiceName() string
	LogLevel() zerolog.Level
	Env() string
	CommitHash() string
}

func Init(cfg Cfg) *zerolog.Logger {
	zerolog.ErrorStackMarshaler = MarshalStack

	if cfg.Env() == "local" {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	}

	loggerCtx := log.With().
		Str("svc", cfg.ServiceName()).
		Str("env", cfg.Env()).
		Str("commitHash", cfg.CommitHash()).
		Stack()

	if cfg.LogLevel() <= highestLevelWithCallerInfo {
		loggerCtx = loggerCtx.Caller()
	}

	logger := loggerCtx.Logger().Level(cfg.LogLevel())

	// update the zerolog global logger
	log.Logger = logger

	return &log.Logger
}

func MarshalStack(err error) interface{} {
	type stackTracer interface {
		StackTrace() errors.StackTrace
	}
	sterr, ok := err.(stackTracer)
	if !ok {
		return nil
	}
	st := sterr.StackTrace()
	out := make([]map[string]string, 0, len(st))
	for _, frame := range st {
		file, line, name := errutils.StackTraceInfo(frame)
		out = append(out, map[string]string{
			stackSourceFileName:     file,
			stackSourceLineName:     line,
			stackSourceFunctionName: name,
		})
	}
	return out
}
