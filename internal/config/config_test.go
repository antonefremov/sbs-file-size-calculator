package config_test

import (
	"net/http"
	"os"
	"testing"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/router"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/config"
)

var envVars = map[string]string{
	"SERVICE_NAME":                 "test",
	"API_VERSION":                  "v1",
	"LOG_LEVEL":                    "debug",
	"ENV":                          "test",
	"HTTP_PORT":                    "9000",
	"HTTP_SHUTDOWN_TIMEOUT_SEC":    "14",
	"HTTP_READ_TIMEOUT_SEC":        "16",
	"HTTP_WRITE_TIMEOUT_SEC":       "18",
	"HTTP_IDLE_TIMEOUT_SEC":        "20",
	"HTTP_READ_HEADER_TIMEOUT_SEC": "10",
	"CORS_ALLOWED_ORIGINS":         "*",
	"CORS_ALLOWED_HEADERS":         "Content-Type,X-Correlation-Id",
	"CORS_ALLOWED_METHODS":         "DELETE,GET,HEAD,PATCH,POST,PUT,OPTIONS",
	"TRACING_ENABLED":              "true",
}

func setupCustomConfig(t *testing.T) {
	for key, value := range envVars {
		err := os.Setenv(key, value)
		require.NoError(t, err)
	}
}

func teardown(t *testing.T) {
	for key := range envVars {
		err := os.Unsetenv(key)
		if err != nil {
			t.Logf("Error occurred during test teardown: %+v", err)
		}
	}
}

func TestNewConfigDefault(t *testing.T) {
	// make sure there are no custom config env vars
	teardown(t)

	cfg, err := config.NewConfig("unknown")
	require.NoError(t, err)
	assert.NotNil(t, cfg)

	assert.Equal(t, "sbs-file-size-calculator", cfg.ServiceName())
	assert.Equal(t, "v2", cfg.APIVersion())
	assert.Equal(t, zerolog.InfoLevel, cfg.LogLevel())
	assert.Equal(t, config.DevEnv, cfg.Env())
	assert.Equal(t, ":8080", cfg.HTTPAddress())
	assert.Equal(t, 10, cfg.HTTPReadHeaderTimeout())
	assert.Equal(t, 10, cfg.HTTPShutdownTimeout())
	assert.Equal(t, 10, cfg.HTTPReadTimeout())
	assert.Equal(t, 10, cfg.HTTPWriteTimeout())
	assert.Equal(t, 60, cfg.HTTPIdleTimeout())
	assert.Equal(t, router.CORSSettings{}, cfg.CORSSettings())
	assert.Equal(t, "unknown", cfg.CommitHash())
	assert.Equal(t, false, cfg.TracingEnabled())
}

func TestNewConfigCustom(t *testing.T) {
	setupCustomConfig(t)

	cfg, err := config.NewConfig("unknown")
	require.NoError(t, err)
	assert.NotNil(t, cfg)

	assert.Equal(t, "test", cfg.ServiceName())
	assert.Equal(t, "v1", cfg.APIVersion())
	assert.Equal(t, zerolog.DebugLevel, cfg.LogLevel())
	assert.Equal(t, config.TestEnv, cfg.Env())
	assert.Equal(t, ":9000", cfg.HTTPAddress())
	assert.Equal(t, 10, cfg.HTTPReadHeaderTimeout())
	assert.Equal(t, 14, cfg.HTTPShutdownTimeout())
	assert.Equal(t, 16, cfg.HTTPReadTimeout())
	assert.Equal(t, 18, cfg.HTTPWriteTimeout())
	assert.Equal(t, 20, cfg.HTTPIdleTimeout())

	expectedCORSSettings := router.CORSSettings{
		AllowedOrigins: []string{"*"},
		AllowedHeaders: []string{"Content-Type", "X-Correlation-Id"},
		AllowedMethods: []string{
			http.MethodDelete, http.MethodGet, http.MethodHead,
			http.MethodPatch, http.MethodPost, http.MethodPut,
			http.MethodOptions,
		},
	}
	assert.Equal(t, expectedCORSSettings, cfg.CORSSettings())
	assert.Equal(t, "unknown", cfg.CommitHash())
	assert.Equal(t, true, cfg.TracingEnabled())

	teardown(t)
}
