package config

import (
	"fmt"
	"strings"

	"github.com/Netflix/go-env"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/rs/zerolog"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/router"
)

const (
	// DevEnv represents the dev environment
	DevEnv string = "dev"
	// TestEnv represents the test environment
	TestEnv string = "test"
	// ProdEnv represents the dev environment
	ProdEnv string = "prod"

	// LogLevelError allows log entries up to the Error level
	LogLevelError string = "error"
	// LogLevelInfo allows log entries up to the Info level
	LogLevelInfo string = "info"
	// LogLevelDebug allows log entries up to the Debug level
	LogLevelDebug string = "debug"
)

type envConfig struct {
	ServiceName string `env:"SERVICE_NAME,default=sbs-file-size-calculator"`
	LogLevel    string `env:"LOG_LEVEL,default=info"`
	Env         string `env:"ENV,default=dev"` // service environment: dev, test, prod

	APIVersion string `env:"API_VERSION,default=v2"`

	HTTPHost                 string `env:"HTTP_HOST"`                               // HTTP server host
	HTTPPort                 string `env:"HTTP_PORT,default=8080"`                  // HTTP server port
	HTTPShutdownTimeoutSec   int    `env:"HTTP_SHUTDOWN_TIMEOUT_SEC,default=10"`    // HTTP server shutdown timeout in sec
	HTTPReadTimeoutSec       int    `env:"HTTP_READ_TIMEOUT_SEC,default=10"`        // HTTP request read timeout in sec
	HTTPWriteTimeoutSec      int    `env:"HTTP_WRITE_TIMEOUT_SEC,default=10"`       // HTTP response write timeout in sec
	HTTPIdleTimeoutSec       int    `env:"HTTP_IDLE_TIMEOUT_SEC,default=60"`        // HTTP time (in sec) to wait for the next request when keep-alives are enabled
	HTTPReadHeaderTimeoutSec int    `env:"HTTP_READ_HEADER_TIMEOUT_SEC,default=10"` // HTTP time (in sec) allowed to read request headers

	CORSAllowedHeaders string `env:"CORS_ALLOWED_HEADERS"`
	CORSAllowedMethods string `env:"CORS_ALLOWED_METHODS"`
	CORSAllowedOrigins string `env:"CORS_ALLOWED_ORIGINS"`

	// Tracing
	TracingEnabled bool `env:"TRACING_ENABLED,default=false"`
}

func (ec envConfig) Validate() error {
	return validation.ValidateStruct(&ec,
		validation.Field(&ec.ServiceName, validation.Required, validation.Length(1, 50), is.LowerCase),
		validation.Field(&ec.LogLevel, validation.In(LogLevelError, LogLevelInfo, LogLevelDebug)),
		validation.Field(&ec.Env, validation.In(DevEnv, TestEnv, ProdEnv)),

		validation.Field(&ec.APIVersion, validation.Required, validation.Length(1, 6), is.LowerCase),

		validation.Field(&ec.HTTPHost, validation.Length(0, 20)),
		validation.Field(&ec.HTTPPort, validation.Required, validation.Length(1, 4)),
		validation.Field(&ec.HTTPShutdownTimeoutSec, validation.Required, validation.Min(1)),
		validation.Field(&ec.HTTPReadTimeoutSec, validation.Required, validation.Min(1)),
		validation.Field(&ec.HTTPWriteTimeoutSec, validation.Required, validation.Min(1)),
		validation.Field(&ec.HTTPIdleTimeoutSec, validation.Required, validation.Min(1)),
		validation.Field(&ec.HTTPReadHeaderTimeoutSec, validation.Required, validation.Min(1)),
	)
}

func loadEnvConfig() (*envConfig, error) {
	cfg := &envConfig{}
	_, err := env.UnmarshalFromEnviron(cfg)
	return cfg, err
}

type Config interface {
	ServiceName() string
	LogLevel() zerolog.Level
	Env() string
	APIVersion() string
	HTTPAddress() string
	HTTPShutdownTimeout() int
	HTTPReadTimeout() int
	HTTPWriteTimeout() int
	HTTPReadHeaderTimeout() int
	HTTPIdleTimeout() int
	CORSSettings() router.CORSSettings
	CommitHash() string
	MarshalZerologObject(*zerolog.Event)
	TracingEnabled() bool
}

type config struct {
	// TODO: refactor, it should not be envConf, it's a config that read from env var and cli flags
	envConfig  *envConfig
	commitHash string
}

func NewConfig(commitHash string) (Config, error) {
	cfg, err := loadEnvConfig()
	if err != nil {
		return nil, err
	}

	err = cfg.Validate()
	if err != nil {
		return nil, err
	}

	return &config{envConfig: cfg, commitHash: commitHash}, nil
}

func (cfg *config) MarshalZerologObject(e *zerolog.Event) {
	e.Interface("cfg", cfg.envConfig)
}

func (cfg *config) ServiceName() string {
	return cfg.envConfig.ServiceName
}

func (cfg *config) APIVersion() string {
	return cfg.envConfig.APIVersion
}

func (cfg *config) Env() string {
	return cfg.envConfig.Env
}

func (cfg *config) LogLevel() zerolog.Level {
	lvl, err := zerolog.ParseLevel(strings.ToLower(cfg.envConfig.LogLevel))
	if err != nil {
		lvl = zerolog.InfoLevel
	}
	return lvl
}

func (cfg *config) HTTPAddress() string {
	httpAddress := fmt.Sprintf("%v:%v", cfg.envConfig.HTTPHost, cfg.envConfig.HTTPPort)
	return httpAddress
}

func (cfg *config) HTTPShutdownTimeout() int {
	return cfg.envConfig.HTTPShutdownTimeoutSec
}

func (cfg *config) HTTPReadTimeout() int {
	return cfg.envConfig.HTTPReadTimeoutSec
}

func (cfg *config) HTTPWriteTimeout() int {
	return cfg.envConfig.HTTPWriteTimeoutSec
}

func (cfg *config) HTTPIdleTimeout() int {
	return cfg.envConfig.HTTPIdleTimeoutSec
}

func (cfg *config) HTTPReadHeaderTimeout() int {
	return cfg.envConfig.HTTPReadHeaderTimeoutSec
}

func (cfg *config) CORSSettings() router.CORSSettings {
	var s router.CORSSettings
	dlm := ","

	if a := strings.TrimSpace(cfg.envConfig.CORSAllowedOrigins); len(a) > 0 {
		s.AllowedOrigins = strings.Split(a, dlm)
	}

	if a := strings.TrimSpace(cfg.envConfig.CORSAllowedHeaders); len(a) > 0 {
		s.AllowedHeaders = strings.Split(a, dlm)
	}

	if a := strings.TrimSpace(cfg.envConfig.CORSAllowedMethods); len(a) > 0 {
		s.AllowedMethods = strings.Split(a, dlm)
	}

	return s
}

func (cfg *config) CommitHash() string {
	return cfg.commitHash
}

func (cfg *config) TracingEnabled() bool {
	return cfg.envConfig.TracingEnabled
}
