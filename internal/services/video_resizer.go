package services

//go:generate go run github.com/vektra/mockery/v2/ --name=VideoResizeService --case=Snake

import (
	"log"
	"net"
	"net/http"
	"net/url"
	"time"

	"github.com/patrickmn/go-cache"
	"github.com/rs/zerolog"
	ffmpeg "github.com/u2takey/ffmpeg-go"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/model"
)

type VideoResizeIn struct {
	URL *url.URL
}

type VideoResizeOut struct {
	Size     int
	Category model.Category
	MaxAge   string
}

type VideoResizeService interface {
	Resize(VideoResizeIn, *zerolog.Logger) (VideoResizeOut, error)
}

type videoResizeService struct {
	client *http.Client
	cache  *cache.Cache
}

func NewVideoResizeService() VideoResizeService {
	transport := &http.Transport{
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
			DualStack: true,
		}).DialContext,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
	}
	client := &http.Client{
		Timeout:   time.Second * 10,
		Transport: transport,
	}

	return &videoResizeService{
		client: client,
		cache:  cache.New(cacheExpiration*time.Minute, cacheCleanup*time.Minute),
	}
}

func (s *videoResizeService) Resize(in VideoResizeIn, l *zerolog.Logger) (VideoResizeOut, error) {
	// var result VideoResizeOut

	// cachedResult, found := s.cache.Get(destURL)
	// if found {
	// 	var ok bool
	// 	if result, ok = cachedResult.(VideoResizeOut); ok {
	// 		// found the requested item in cache and omitting the network request
	// 		return result, nil
	// 	}
	// }

	err := ffmpeg.Input("./sample_data/in1.mp4", ffmpeg.KwArgs{"ss": 1}).
		Output("./sample_data/out1.mp4", ffmpeg.KwArgs{"t": 2}).OverWriteOutput().Run()
	if err != nil {
		log.Fatal("Error calling ffmpeg")
	}

	// return result, nil
	return VideoResizeOut{}, nil
}
