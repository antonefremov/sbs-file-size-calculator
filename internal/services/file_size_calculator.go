package services

//go:generate go run github.com/vektra/mockery/v2/ --name=FileSizeCalcService --case=Snake

import (
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"path"
	"time"

	"github.com/patrickmn/go-cache"
	"github.com/rs/zerolog"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/errutils"
	"gitlab.com/antonefremov/sbs-file-size-calculator/internal/common/model"
)

const (
	cacheExpiration = 5
	cacheCleanup    = 10
)

type FileSizeCalcIn struct {
	URL *url.URL
}

type FileSizeCalcOut struct {
	Size     int
	Category model.Category
	MaxAge   string
}

type FileSizeCalcService interface {
	Calculate(FileSizeCalcIn, *zerolog.Logger) (FileSizeCalcOut, error)
}

type fileSizeCalcService struct {
	client *http.Client
	cache  *cache.Cache
}

func NewFileSizeCalcService() FileSizeCalcService {
	transport := &http.Transport{
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
			DualStack: true,
		}).DialContext,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
	}
	client := &http.Client{
		Timeout:   time.Second * 10,
		Transport: transport,
	}

	return &fileSizeCalcService{
		client: client,
		cache:  cache.New(cacheExpiration*time.Minute, cacheCleanup*time.Minute),
	}
}

func (s *fileSizeCalcService) Calculate(in FileSizeCalcIn, l *zerolog.Logger) (FileSizeCalcOut, error) {
	var result FileSizeCalcOut
	destURL := fmt.Sprintf("%s://%s", in.URL.Scheme, path.Join(in.URL.Host, in.URL.Path))

	cachedResult, found := s.cache.Get(destURL)
	if found {
		var ok bool
		if result, ok = cachedResult.(FileSizeCalcOut); ok {
			// found the requested item in cache and omitting the network request
			return result, nil
		}
	}

	req := &http.Request{
		Method: http.MethodGet,
		URL:    in.URL,
	}
	resp, err := s.client.Do(req)
	if err != nil {
		deErr := errutils.DownstreamError{
			Msg:     err.Error(),
			Err:     err,
			DestURL: destURL,
		}
		l.Err(err).Send()
		return result, &deErr
	}
	defer func() {
		if resp != nil && resp.Body != nil {
			if err = resp.Body.Close(); err != nil {
				l.Err(err).Send()
			}
		}
	}()

	if resp.StatusCode >= 400 {
		respBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			l.Err(err).Msg("response status was not successful, no error response provided")
		}
		deErr := errutils.DownstreamError{
			Msg:     string(respBody),
			Err:     fmt.Errorf("response status was not successful: %d", resp.StatusCode),
			DestURL: destURL,
		}
		l.Err(&deErr).Send()
		return result, &deErr
	}

	// I believe it should save memory instead of reading resp.Body
	result.Size = int(resp.ContentLength)
	switch {
	case result.Size <= 1000:
		result.Category = model.FileSizeCategorySmall
	case result.Size > 1000 && result.Size <= 3000:
		result.Category = model.FileSizeCategoryMedium
	case result.Size > 3000 && result.Size <= 5000:
		result.Category = model.FileSizeCategoryLarge
	default:
		result.Category = model.FileSizeCategoryXLarge
	}
	// set the cache header value
	result.MaxAge = fmt.Sprintf("max-age=%d", cacheExpiration*60)

	// save the result into cache
	s.cache.Set(
		destURL,
		result,
		cache.DefaultExpiration,
	)

	return result, nil
}
