## Deliverables

- [X] Go code
- [X] API documentation (please see the swagger file)
- [X] Developer documentation (this file)
- [X] Git repository
- [ ] Infra as code (ready 90% - the Service creation automation is not in place)
- [X] Pipeline (please see the .gitlab-ci.yml file)
- [X] Implemented Caching

## Core Infrastructure setup

### Config the account

```bash
aws configure
AWS Access Key ID [None]: <key_id>
AWS Secret Access Key [None]: <access_key>
Default region name [None]: ap-southeast-2
Default output format [None]: json
```

### Create core AWS infrastructure

* VPC
* 2 subnets in 2 different AZs
* Internet Gateway
* routing tables

```bash
aws cloudformation create-stack --capabilities CAPABILITY_IAM --stack-name ecs-sbs-core-infrastructure --template-body file://./core-infrastructure.yml
```

### Create ECS Cluster via CloudFormation template

Launchtype _Fargate_:
```bash
aws cloudformation create-stack --stack-name ecs-sbs-fargate --capabilities CAPABILITY_IAM --template-body file://./ecs-fargate.yml
```

### Create ALB, via Cloudformation template:

```bash
aws cloudformation create-stack \
--stack-name external-alb-sbs \
--template-body file://./alb-external.yml
```

### Create Task Definitions with service discovery

```bash
aws ecs register-task-definition --cli-input-json file://td-calcapp-setup.json --region ap-southeast-2
```

### Create Services based on the created task definitions

The AWS ECS service is created manually. Needs to be automated.

## Local run
```bash
go run main.go
```

## Local Docker run
```bash
ci make drun
```

## TODO List
- [ ] Build V1 version of the API
- [ ] Normalise the error messages to be in format `{"error": "human readable description of the error"}`
- [ ] Infra as code (finish the AWS Service instance creation automation)
- [ ] Fix the `ci make deploy` command so that it deploys the whole project
- [ ] Increase unit tests coverage (e.g. the internal/services/file_size_calculator.go)
- [ ] Add tracing
- [ ] Minimise the Docker image
- [ ] Add integration tests using external client such as Postman/Insomnia
